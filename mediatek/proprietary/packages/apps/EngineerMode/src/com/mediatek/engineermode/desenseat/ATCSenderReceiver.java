package com.mediatek.engineermode.desenseat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.ModemCategory;

/**
 * Receiver for sending AT command.
 *
 */
public class ATCSenderReceiver extends BroadcastReceiver {

    private static final String TAG = "ATCSenderReceiver";
    public static final String ATC_SEND_ACTION = "com.mediatek.engineermode.atc_send";
    public static final String ATC_DONE_ACTION = "com.mediatek.engineermode.atc_done";
    public static final String EXTRA_CMD = "atc_send.cmd";

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        if (ATC_SEND_ACTION.equals(intent.getAction())) {
            String param = intent.getStringExtra(EXTRA_CMD);
            Elog.d(TAG, "receive broadcast: ATC_SEND_ACTION and param is " + param);
            String[] cmd = new String[2];
            cmd[0] = param;
            cmd[1] = "";

            Phone phone;
            if (TelephonyManager.getDefault().getPhoneCount() > 1) {
                phone = PhoneFactory.getPhone(PhoneConstants.SIM_ID_1);
            } else {
                phone = PhoneFactory.getDefaultPhone();
            }

            if (FeatureSupport.isSupported(FeatureSupport.FK_MTK_C2K_SUPPORT)) {
                if (FeatureSupport.isSupported(FeatureSupport.FK_MTK_SVLTE_SUPPORT)) {
                    phone = ModemCategory.getCdmaPhone();
                }
                if (FeatureSupport.isSupported(FeatureSupport.FK_EVDO_DT_SUPPORT)
                        && phone.getPhoneType() == PhoneConstants.PHONE_TYPE_CDMA) {
                    phone = PhoneFactory.getPhone(PhoneConstants.SIM_ID_2);
                }
            }

            phone.invokeOemRilRequestStrings(cmd, null);
            Elog.d(TAG, "invokeOemRilRequestStrings done");
            Intent inDone = new Intent(ATCSenderReceiver.ATC_DONE_ACTION);
            context.sendBroadcast(inDone);
        }
    }

}
