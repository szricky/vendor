/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "PLAT"

#include "common/hdr/Platform.h"

#ifdef USE_PERFSERVICE
#include <perfservicenative/PerfServiceNative.h>
#endif

#ifdef USE_AFFINITY
#include <common/hdr/utils/Debug.h>
#include <linux/mt_sched.h>
#endif

using namespace NSCam;

// ---------------------------------------------------------------------------

ANDROID_SINGLETON_STATIC_INSTANCE(Platform);

class PerfImpl final : public IPerf
{
public:
    PerfImpl();
    virtual ~PerfImpl();

    int enableAffinity(pid_t pid);
    int disableAffinity(pid_t pid);

private:
    int mSceneHandle;
};

#ifdef USE_PERFSERVICE

PerfImpl::PerfImpl()
    : mSceneHandle(-1)
{
    HDR_TRACE_CALL();

    mSceneHandle = PerfServiceNative_userRegScn();
    if (mSceneHandle == -1)
    {
        HDR_LOGE("register PerfService scenario failed");
        goto lbExit;
    }

    // config LL core (CA53 x4 up to 1.5G)
    PerfServiceNative_userRegScnConfig(
        mSceneHandle, CMD_SET_CLUSTER_CPU_CORE_MIN, 0, 4, 0, 0);        // 4 x L
    PerfServiceNative_userRegScnConfig(
        mSceneHandle, CMD_SET_CLUSTER_CPU_FREQ_MIN, 0, 1391000, 0, 0);  // L freq = 1.391G

    // config L core (CA53 x4 up to 1.95G)
    PerfServiceNative_userRegScnConfig(
        mSceneHandle, CMD_SET_CLUSTER_CPU_CORE_MIN, 1, 4, 0, 0);        // 4 x L
    PerfServiceNative_userRegScnConfig(
        mSceneHandle, CMD_SET_CLUSTER_CPU_FREQ_MIN, 1, 1950000, 0, 0);  // L freq = 1.95G

    // config big core (CA72 x2 up to 2.288G)
    PerfServiceNative_userRegScnConfig(
        mSceneHandle, CMD_SET_CLUSTER_CPU_CORE_MIN, 2, 2, 0, 0);        // 2 x BIG
    PerfServiceNative_userRegScnConfig(
        mSceneHandle, CMD_SET_CLUSTER_CPU_FREQ_MIN, 2, 2288000, 0, 0);  // Big freq = 2.288G

    PerfServiceNative_userEnable(mSceneHandle);

lbExit:
    return;
}

PerfImpl::~PerfImpl()
{
    HDR_TRACE_CALL();

    MBOOL ret = MTRUE;

    if (mSceneHandle != -1)
    {
        PerfServiceNative_userDisable(mSceneHandle);
        PerfServiceNative_userUnreg(mSceneHandle);
    }

lbExit:
    return;
}

#else

PerfImpl::PerfImpl()
    : mSceneHandle(-1)
{
}

PerfImpl::~PerfImpl()
{
}

#endif // #ifdef USE_PERFSERVICE

int PerfImpl::enableAffinity(pid_t pid)
{
    int status = 0;

#ifdef USE_AFFINITY
    // 2 big cores + 4 L cores + 4 LL cores
    int cpuMask = 0x3FF;
    // put current thread to bigger cores
    cpu_set_t cpuset;
    unsigned int mask, cpu_no;
    CPU_ZERO(&cpuset);
    for (mask = 1, cpu_no = 0; mask < 0x3FF; mask <<= 1, cpu_no++)
    {
        if (mask & cpuMask)
        {
            CPU_SET(cpu_no, &cpuset);
        }
    }
    status = mt_sched_setaffinity(pid, sizeof(cpu_set_t), &cpuset);
    HDR_LOGE_IF(status, "mt_sched_setaffinity() failed: status(%d)", status);
#endif

    return status;
}

int PerfImpl::disableAffinity(pid_t pid)
{
    int status = 0;

#ifdef USE_AFFINITY
    status = mt_sched_exitaffinity(pid);
    HDR_LOGE_IF(status, "mt_sched_exitaffinity() failed: status(%d)", status);
#endif

    return status;
}

// ---------------------------------------------------------------------------

sp<IPerf> Platform::getPerf()
{
    return new PerfImpl();
}
