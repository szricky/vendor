/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "StereoSizeProvider"

#include <math.h>
//
#include <metadata/IMetadataProvider.h>
#include <metadata/client/mtk_metadata_tag.h>
//
#include <vsdof/hal/stereo_setting_provider.h>
#include <vsdof/hal/stereo_size_provider.h>
#include "pass2/pass2A_size_providers.h"

using android::Mutex;
#define LOG_TAG "StereoSizeProvider"

#define STEREO_SIZE_PROVIDER_DEBUG

#ifdef STEREO_SIZE_PROVIDER_DEBUG    // Enable debug log.

#undef __func__
#define __func__ __FUNCTION__

#ifndef GTEST
#define MY_LOGD(fmt, arg...)    CAM_LOGD("[%s]" fmt, __func__, ##arg)
#define MY_LOGI(fmt, arg...)    CAM_LOGI("[%s]" fmt, __func__, ##arg)
#define MY_LOGW(fmt, arg...)    CAM_LOGW("[%s] WRN(%5d):" fmt, __func__, __LINE__, ##arg)
#define MY_LOGE(fmt, arg...)    CAM_LOGE("[%s] %s ERROR(%5d):" fmt, __func__,__FILE__, __LINE__, ##arg)
#else
#define MY_LOGD(fmt, arg...)    printf("[D][%s]" fmt"\n", __func__, ##arg)
#define MY_LOGI(fmt, arg...)    printf("[I][%s]" fmt"\n", __func__, ##arg)
#define MY_LOGW(fmt, arg...)    printf("[W][%s] WRN(%5d):" fmt"\n", __func__, __LINE__, ##arg)
#define MY_LOGE(fmt, arg...)    printf("[E][%s] %s ERROR(%5d):" fmt"\n", __func__,__FILE__, __LINE__, ##arg)
#endif

#else   // Disable debug log.
#define MY_LOGD(a,...)
#define MY_LOGI(a,...)
#define MY_LOGW(a,...)
#define MY_LOGE(a,...)
#endif  // STEREO_SIZE_PROVIDER_DEBUG

//===============================================================
//  Singleton and init operations
//===============================================================
Mutex StereoSizeProvider::mLock;

StereoSizeProvider *
StereoSizeProvider::getInstance()
{
    Mutex::Autolock lock(mLock);
    static StereoSizeProvider _instance;
    return &_instance;
}

StereoSizeProvider::StereoSizeProvider()
{
    m_captureSize = Pass2A_SizeProvider::instance()->sizeInfo(eSTEREO_SCENARIO_CAPTURE).areaWDMA.size;
}

bool
StereoSizeProvider::getPass1Size( ENUM_STEREO_SENSOR sensor,
                                  EImageFormat format,
                                  EPortIndex port,
                                  ENUM_STEREO_SCENARIO scenario,
                                  MRect &tgCropRect,
                                  MSize &outSize,
                                  MUINT32 &strideInBytes
                                ) const
{
    // Get sensor senario
    int sensorScenario = getSensorSenario(scenario);

    // Prepare sensor hal
    IHalSensorList* sensorList = IHalSensorList::get();
    if(NULL == sensorList) {
        MY_LOGE("Cannot get sensor list");
        return false;
    }

    MINT32 err = 0;
    int32_t main1Idx, main2Idx;
    StereoSettingProvider::getStereoSensorIndex(main1Idx, main2Idx);
    int sendorDevIndex = sensorList->querySensorDevIdx((eSTEREO_SENSOR_MAIN1 == sensor) ? main1Idx : main2Idx);
    IHalSensor* pIHalSensor = sensorList->createSensor(LOG_TAG, (eSTEREO_SENSOR_MAIN1 == sensor) ? main1Idx : main2Idx);
    if(NULL == pIHalSensor) {
        MY_LOGE("Cannot get hal sensor");
        return false;
    }

    SensorStaticInfo sensorStaticInfo;
    memset(&sensorStaticInfo, 0, sizeof(SensorStaticInfo));
    sensorList->querySensorStaticInfo(sendorDevIndex, &sensorStaticInfo);

    StereoArea result;
    tgCropRect.p.x = 0;
    tgCropRect.p.y = 0;
    tgCropRect.s.w = (scenario == eSTEREO_SCENARIO_CAPTURE) ? sensorStaticInfo.captureWidth  : sensorStaticInfo.previewWidth;
    tgCropRect.s.h = (scenario == eSTEREO_SCENARIO_CAPTURE) ? sensorStaticInfo.captureHeight : sensorStaticInfo.previewHeight;
    outSize.w = tgCropRect.s.w;
    outSize.h = tgCropRect.s.h;

    if(EPortIndex_RRZO == port) {
        outSize.w = sensorStaticInfo.previewWidth;
        outSize.h = sensorStaticInfo.previewHeight;
    }

    STEREO_RATIO_E currentRatio = (EPortIndex_IMGO == port) ? eRatio_4_3 : StereoSettingProvider::imageRatio();
    //TODO: uncomment this line for de-noise sizes
    // STEREO_RATIO_E currentRatio = StereoSettingProvider::imageRatio();
    if(eRatio_Sensor != currentRatio) {
        int n = 1, m = 1;   // h = w * n/m
        switch(currentRatio) {
            case eRatio_16_9:
            default:
                n = 9;
                m = 16;
                break;
            case eRatio_4_3:
                n = 3;
                m = 4;
                break;
        }

        outSize.h = (outSize.w * n / m) & ~1;   //Size must be even
        int height = tgCropRect.s.h;
        tgCropRect.s.h = (tgCropRect.s.w * n / m) & ~1; //Size must be even
        if(height < tgCropRect.s.h) {
            tgCropRect.s.h = height;
            outSize.h      = height;
        } else {
            tgCropRect.p.y = (height - tgCropRect.s.h)/2;
        }
    }

    //Get FPS
    int defaultFPS = 0; //result will be 10xFPS, e.g. if 30 fps, defaultFPS = 300
    err = pIHalSensor->sendCommand(sendorDevIndex, SENSOR_CMD_GET_DEFAULT_FRAME_RATE_BY_SCENARIO,
                                   (MINTPTR)&sensorScenario, (MINTPTR)&defaultFPS, 0);
    if(err) {
        MY_LOGE("Cannot get default frame rate");
        return false;
    }

    //Get pixel format
    E_ISP_PIXMODE pixelMode;
    defaultFPS /= 10;
    err = pIHalSensor->sendCommand(sendorDevIndex, SENSOR_CMD_GET_SENSOR_PIXELMODE,
                                   (MINTPTR)&sensorScenario, (MINTPTR)&defaultFPS, (MINTPTR)&pixelMode);
    if(err) {
        MY_LOGE("Cannot get pixel mode");
        return false;
    }

    NSImageio::NSIspio::ISP_QUERY_RST queryRst;
    NSImageio::NSIspio::ISP_QuerySize( port,
                                       NSImageio::NSIspio::ISP_QUERY_X_PIX
                                       | NSImageio::NSIspio::ISP_QUERY_STRIDE_PIX
                                       | NSImageio::NSIspio::ISP_QUERY_STRIDE_BYTE,
                                       format,
                                       outSize.w,
                                       queryRst,
                                       pixelMode
                                     );

    strideInBytes = queryRst.stride_byte;

    tgCropRect.p.x = (outSize.w - queryRst.x_pix)/2 * tgCropRect.s.w / outSize.w;
    outSize.w = queryRst.x_pix;

    pIHalSensor->destroyInstance(LOG_TAG);
    return true;
}

template <typename T>
inline MBOOL
tryGetMetadata(
    IMetadata const* pMetadata,
    MUINT32 const tag,
    T & rVal
)
{
    if (pMetadata == NULL) {
        MY_LOGW("pMetadata == NULL");
        return MFALSE;
    }
    //
    IMetadata::IEntry entry = pMetadata->entryFor(tag);
    if(!entry.isEmpty()) {
        rVal = entry.itemAt(0, Type2Type<T>());
        return MTRUE;
    }
    //
    return MFALSE;
}

bool
StereoSizeProvider::getPass1ActiveArrayCrop(ENUM_STEREO_SENSOR sensor, MRect &cropRect)
{
    int main1Id, main2Id;
    StereoSettingProvider::getStereoSensorIndex(main1Id, main2Id);
    int sensorId = -1;
    switch(sensor)
    {
        case eSTEREO_SENSOR_MAIN1:
            sensorId = main1Id;
            break;
        case eSTEREO_SENSOR_MAIN2:
            sensorId = main2Id;
            break;
        default:
            break;
    }

    if(sensorId < 0) {
        MY_LOGW("Wrong sensor: %d", sensorId);
        return false;
    }

    sp<IMetadataProvider> pMetadataProvider = NSMetadataProviderManager::valueFor(sensorId);
    if( ! pMetadataProvider.get() ) {
        MY_LOGE("MetadataProvider is NULL");
        return false;
    }

    bool result = false;
    IMetadata static_meta = pMetadataProvider->geMtktStaticCharacteristics();
    if( tryGetMetadata<MRect>(&static_meta, MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION, cropRect) ) {
        cropRect.s.w = (cropRect.s.w>>1)<<1;
        cropRect.s.h = (cropRect.s.h>>1)<<1;
        if(eRatio_16_9 == StereoSettingProvider::imageRatio()) {
            int newHeight = cropRect.s.h;
            newHeight = ((cropRect.s.w * 9 / 16)>>1)<<1;
            cropRect.p.y = (cropRect.s.h-newHeight)/2;
            cropRect.s.h = newHeight;
        }
        result = true;
    }

    return result;
}

bool
StereoSizeProvider::getPass2SizeInfo(ENUM_PASS2_ROUND round, ENUM_STEREO_SCENARIO eScenario, Pass2SizeInfo &pass2SizeInfo)
{
    bool isSuccess = true;
    switch(round) {
        case PASS2A:
            pass2SizeInfo = Pass2A_SizeProvider::instance()->sizeInfo(eScenario);
            if(eSTEREO_SCENARIO_CAPTURE == eScenario) {
                pass2SizeInfo.areaWDMA.size = m_captureSize;
            }
            break;
        case PASS2A_2:
            pass2SizeInfo = Pass2A_2_SizeProvider::instance()->sizeInfo(eScenario);
            break;
        case PASS2A_3:
            pass2SizeInfo = Pass2A_3_SizeProvider::instance()->sizeInfo(eScenario);
            break;
        case PASS2A_P:
            pass2SizeInfo = Pass2A_P_SizeProvider::instance()->sizeInfo(eScenario);
            break;
        case PASS2A_P_2:
            pass2SizeInfo = Pass2A_P_2_SizeProvider::instance()->sizeInfo(eScenario);
            break;
        case PASS2A_P_3:
            pass2SizeInfo = Pass2A_P_3_SizeProvider::instance()->sizeInfo(eScenario);
            break;
        default:
            isSuccess = false;
    }

    return isSuccess;
}

StereoArea
StereoSizeProvider::getBufferSize(ENUM_BUFFER_NAME eName, ENUM_STEREO_SCENARIO eScenario) const
{
    switch(eName) {
        //N3D before MDP for capture
        case E_MV_Y_LARGE:
        case E_MASK_M_Y_LARGE:
        case E_SV_Y_LARGE:
        case E_MASK_S_Y_LARGE:
            return StereoArea(MSize(2176, 1504), MSize(256, 64), MPoint(128, 32))
                   .applyRatio(StereoSettingProvider::imageRatio())
                   .rotatedByModule()
                   .apply16Align();
            break;

        //N3D Output
        case E_MV_Y:
        case E_MASK_M_Y:
        case E_SV_Y:
        case E_MASK_S_Y:

        //DPE Output
        case E_DMP_H:
        case E_CFM_H:
        case E_RESPO:
        case E_LDC:
            {
                StereoArea area(MSize(272, 188), MSize(32,  8), MPoint(16, 4));
                if(StereoSettingProvider::isDeNoise() &&
                   eScenario == eSTEREO_SCENARIO_CAPTURE)
                {
                    area *= 2.0f;
                }

                return area
                       .applyRatio(StereoSettingProvider::imageRatio())
                       .rotatedByModule()
                       .apply2Align();
            }
            break;

        //OCC Output
        case E_MY_S:
        case E_DMH:

        //WMF Output
        case E_DMW:
            {
                StereoArea area(240, 180);
                if(StereoSettingProvider::isDeNoise() &&
                   eScenario == eSTEREO_SCENARIO_CAPTURE)
                {
                    area *= 2.0f;
                }

                return area
                       .applyRatio(StereoSettingProvider::imageRatio())
                       .rotatedByModule()
                       .apply2Align();
            }
            break;

        //GF Output
        case E_DMG:
        case E_DMBG:
            {
                StereoArea area(240, 180);
                if(StereoSettingProvider::isDeNoise() &&
                   eScenario == eSTEREO_SCENARIO_CAPTURE)
                {
                    area *= 2.0f;
                }

                return area
                       .applyRatio(StereoSettingProvider::imageRatio())
                       .apply2Align();
            }
            break;

        case E_DEPTH_MAP:
            switch(CUSTOM_DEPTHMAP_SIZE) {
            case STEREO_DEPTHMAP_1X:
                return StereoArea(240, 180) //16:9 -> 240x136
                       .applyRatio(StereoSettingProvider::imageRatio())
                       .apply2Align(); //small scale only needs to be 2-align
                break;
            case STEREO_DEPTHMAP_2X:
            default:
                return StereoArea(480, 360) //16:9 -> 480x272
                       .applyRatio(StereoSettingProvider::imageRatio())
                       .apply16AlignToContent();
                break;
            case STEREO_DEPTHMAP_4X:
                return StereoArea(960, 720) //16:9 -> 960x544
                       .applyRatio(StereoSettingProvider::imageRatio())
                       .apply16AlignToContent();
                break;
            }
            break;

        //Bokeh Output
        case E_BOKEH_WROT: //Saved image
            switch(eScenario) {
                case eSTEREO_SCENARIO_PREVIEW:
                    return STEREO_AREA_ZERO;
                    break;
                case eSTEREO_SCENARIO_RECORD:   //1920x1088 (16-align)
                    {
                        Pass2SizeInfo pass2SizeInfo;
                        getInstance()->getPass2SizeInfo(PASS2A, eScenario, pass2SizeInfo);
                        return pass2SizeInfo.areaWDMA;
                    }
                    break;
                case eSTEREO_SCENARIO_CAPTURE:  //3072x1728
                    {
                        return StereoArea(m_captureSize);
                    }
                    break;
                default:
                    break;
            }
            break;
        case E_BOKEH_WDMA:
            switch(eScenario) {
                case eSTEREO_SCENARIO_PREVIEW:  //Display
                case eSTEREO_SCENARIO_RECORD:
                    return (eRatio_16_9 == StereoSettingProvider::imageRatio()) ? StereoArea(1920, 1080) : StereoArea(1440, 1080);
                    break;
                case eSTEREO_SCENARIO_CAPTURE:  //Clean image, 3072x1728
                    {
                        return StereoArea(m_captureSize);
                    }
                    break;
                default:
                    break;
            }
            break;
        case E_BOKEH_3DNR:
            switch(eScenario) {
                case eSTEREO_SCENARIO_PREVIEW:
                case eSTEREO_SCENARIO_RECORD:
                    return (eRatio_16_9 == StereoSettingProvider::imageRatio()) ? StereoArea(1920, 1080) : StereoArea(1440, 1080);
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }

    return STEREO_AREA_ZERO;
}

MSize
StereoSizeProvider::getSBSImageSize()
{
    Pass2SizeInfo pass2SizeInfo;
    getPass2SizeInfo(PASS2A_2, eSTEREO_SCENARIO_CAPTURE, pass2SizeInfo);
    MSize result = pass2SizeInfo.areaWDMA.size;
    result.w *= 2;

    return result;
}
