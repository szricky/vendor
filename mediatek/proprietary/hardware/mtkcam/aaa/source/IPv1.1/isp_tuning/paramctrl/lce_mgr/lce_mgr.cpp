/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "lce_mgr"

#ifndef ENABLE_MY_LOG
    #define ENABLE_MY_LOG       (1)
#endif

#include <cutils/properties.h>
#include <aaa_types.h>
#include <aaa_log.h>
#include <camera_custom_nvram.h>
#include <isp_tuning.h>
#include <awb_param.h>
#include <ae_param.h>
#include <af_param.h>
#include <flash_param.h>
#include <isp_tuning_cam_info.h>
#include <isp_tuning_idx.h>
#include <isp_tuning_custom.h>
#include <isp_mgr.h>
#include <isp_mgr_helper.h>
#include "lce_mgr.h"
#include <isp_tuning_mgr.h>
#include "paramctrl_if.h"
#include "paramctrl.h"





using namespace NSIspTuning;
using namespace NSIspTuningv3;
namespace NSIspTuningv3
{


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
template <ESensorDev_T const eSensorDev>
class LceMgrDev : public LceMgr
{
public:
    static
    LceMgr*
    getInstance(ISP_NVRAM_REGISTER_STRUCT& rIspNvramReg, ISP_NVRAM_LCE_TUNING_PARAM_T* pIspNvramLcePara)
    {
        static LceMgrDev<eSensorDev> singleton(rIspNvramReg, pIspNvramLcePara);
        return &singleton;
    }
    virtual MVOID destroyInstance() {}

    LceMgrDev(ISP_NVRAM_REGISTER_STRUCT& rIspNvramReg, ISP_NVRAM_LCE_TUNING_PARAM_T* pIspNvramLcePara)
        : LceMgr(eSensorDev, rIspNvramReg, pIspNvramLcePara)
    {}

    virtual ~LceMgrDev() {}

};

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#define INSTANTIATE(_dev_id) \
    case _dev_id: return  LceMgrDev<_dev_id>::getInstance(rIspNvramReg, pIspNvramLcePara)

LceMgr*
LceMgr::
createInstance(ESensorDev_T const eSensorDev, ISP_NVRAM_REGISTER_STRUCT& rIspNvramReg, ISP_NVRAM_LCE_TUNING_PARAM_T* pIspNvramLcePara)
{

    switch  (eSensorDev)
    {
    INSTANTIATE(ESensorDev_Main);       //  Main Sensor
    INSTANTIATE(ESensorDev_MainSecond); //  Main Second Sensor
    INSTANTIATE(ESensorDev_Sub);        //  Sub Sensor
    default:
        break;
    }

    return  MNULL;
}

LceMgr::LceMgr(ESensorDev_T const eSensorDev, ISP_NVRAM_REGISTER_STRUCT& rIspNvramReg, ISP_NVRAM_LCE_TUNING_PARAM_T* pIspNvramLcePara)
    : m_rIspNvramReg  (rIspNvramReg)
    , m_eSensorDev  (eSensorDev)
    , m_pIspNvramLcePara (pIspNvramLcePara)
    //, m_pGmaAlgo (MTKGma::createInstance())
    , mLceScenario (0) //E_LCE_SCENARIO_PREVIEW
    , mLogEn (0)
    , mbProfileChange(0)
{
    char value[PROPERTY_VALUE_MAX] = {'\0'};
    property_get("debug.lce_algo.enable", value, "1");
    mAlgoEn = atoi(value);

    MY_LOG_IF(1, "[%s()] before MTKLce::createInstance", __FUNCTION__);

    if (mAlgoEn) m_pLceAlgo = MTKLce::createInstance(static_cast<eLCESensorDev_T>(m_eSensorDev));
    else m_pLceAlgo = NULL;

    MY_LOG_IF(1, "[%s()] m_pLceAlgo(%p), m_eSensorDev(%d), m_pIspNvramLcePara(%d)", __FUNCTION__, m_pLceAlgo, m_eSensorDev, m_pIspNvramLcePara);

    if (mAlgoEn) m_pLceAlgo->LceReset();

    MY_LOG_IF(1, "[%s()] after LceReset", __FUNCTION__);

    mEnv = *((MTK_LCE_ENV_INFO_STRUCT*)m_pIspNvramLcePara);

    MY_LOG_IF(1, "[%s()] before LceInit", __FUNCTION__);

    if (mAlgoEn) m_pLceAlgo->LceInit(&mEnv, NULL);

    MY_LOG_IF(1, "[%s()] after LceInit", __FUNCTION__);

}


LceMgr::~LceMgr()
{
   MY_LOG_IF(1, "[%s()] before LceExit (%p)", __FUNCTION__, m_pLceAlgo);
   if (mAlgoEn) m_pLceAlgo->LceExit();
   MY_LOG_IF(1, "[%s()] after GmaExit", __FUNCTION__);
   if (mAlgoEn) m_pLceAlgo->destroyInstance(m_pLceAlgo);
   MY_LOG_IF(1, "[%s()] after destroyInstance (%p)", __FUNCTION__, m_pLceAlgo);
}

MVOID
LceMgr::
printAEInfo(AE_INFO_T const & rAEInfo, const char* username)
{
        MY_LOG("%s: [%s] rAEInfo: %d/%d/%d/%d/%d; %d/%d/%d/%d/%d; %d/%d/%d/%d/%d; %d/%d/%d;"
            , __FUNCTION__
            , username

            , rAEInfo.u4AETarget
            , rAEInfo.u4AECurrentTarget
            , rAEInfo.u4Eposuretime
            , rAEInfo.u4AfeGain
            , rAEInfo.u4IspGain

            , rAEInfo.u4RealISOValue
            , rAEInfo.i4LightValue_x10
            , rAEInfo.u4AECondition
            , rAEInfo.i2FlareOffset
            , rAEInfo.i4GammaIdx

            , rAEInfo.i4LESE_Ratio
            , rAEInfo.u4SWHDR_SE
            , rAEInfo.u4MaxISO
            , rAEInfo.u4AEStableCnt
            , rAEInfo.u4OrgExposuretime

            , rAEInfo.u4OrgRealISOValue
            , rAEInfo.bGammaEnable
            , rAEInfo.u4EVRatio
            );

    /*
        typedef struct {
            MUINT32 u4AETarget;
            MUINT32 u4AECurrentTarget;
            MUINT32 u4Eposuretime;   //!<: Exposure time in ms
            MUINT32 u4AfeGain;           //!<: raw gain
            MUINT32 u4IspGain;           //!<: sensor gain
            MUINT32 u4RealISOValue;
            MINT32  i4LightValue_x10;
            MUINT32 u4AECondition;
            MINT16  i2FlareOffset;
            MINT32  i4GammaIdx;   // next gamma idx
            MINT32  i4LESE_Ratio;    // LE/SE ratio
            MUINT32 u4SWHDR_SE;      //for sw HDR SE ,  -x EV , compare with converge AE
            MUINT32 u4MaxISO;
            MUINT32 u4AEStableCnt;
            MUINT32 u4OrgExposuretime;   //!<: Exposure time in ms
            MUINT32 u4OrgRealISOValue;
            MUINT16 u2Histogrm[GMA_AE_HISTOGRAM_BIN];
            MBOOL bGammaEnable;

            MINT32 i4AEStable;
            MINT32 i4EVRatio;
        } GMA_AE_INFO_T;
    */

        const MUINT32* pHist = &rAEInfo.u4Histogrm[0];
        for (int i=0; i<120; i+=10)
        {
            MY_LOG( "%s: rAEInfo.u4Histogrm[%d-%d]=(%x,%x,%x,%x,%x,  %x,%x,%x,%x,%x)"
                , __FUNCTION__
                , i
                , i+9
                , pHist[i], pHist[i+1], pHist[i+2], pHist[i+3], pHist[i+4]
                , pHist[i+5], pHist[i+6], pHist[i+7], pHist[i+8], pHist[i+9]);
        }
        //(i == 120)
        {
            int i=120;
            MY_LOG( "%s: rAEInfo.u4Histogrm[%d-%d]=(%x,%x,%x,%x,%x,  %x,%x,%x)"
                , __FUNCTION__
                , i
                , i+7
                , pHist[i], pHist[i+1], pHist[i+2], pHist[i+3], pHist[i+4]
                , pHist[i+5], pHist[i+6], pHist[i+7]);
        }

}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID
LceMgr::
stop()
{
    MY_LOG_IF(mLogEn, "[%s()] mLceScenario(%d)", __FUNCTION__, mLceScenario);
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID
LceMgr::
start()
{
#if 0
    char value[PROPERTY_VALUE_MAX] = {'\0'};
    property_get("debug.dynamic_lce.log", value, "0");
    mLogEn = atoi(value);

    mEnv = *((MTK_LCE_ENV_INFO_STRUCT*)m_pIspNvramLcePara);

    if (mAlgoEn) m_pLceAlgo->LceFeatureCtrl(MTKLCE_FEATURE_SET_ENV_INFO, &mEnv, NULL);

    MY_LOG_IF(mLogEn, "[%s()] after MTKLCE_FEATURE_SET_PROC_INFO", __FUNCTION__);
#endif
}




//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID
LceMgr::
calculateLCE(ISP_NVRAM_LCE_T* pLCEReg, NSIspExifDebug::IspExifDebugInfo_T::IspLceInfo* pLceExif, AE_INFO_T const& rAEInfo, EIspProfile_T eIspProfile)
{
    char value[PROPERTY_VALUE_MAX] = {'\0'};
    property_get("debug.dynamic_lce.log", value, "0");
    mLogEn = atoi(value);

    MBOOL fgProfile = MFALSE;

    if((eIspProfile == EIspProfile_IHDR_Preview) ||
       (eIspProfile == EIspProfile_IHDR_Video)   ||
       (eIspProfile == EIspProfile_zHDR_Preview) ||
       (eIspProfile == EIspProfile_zHDR_Video)   ||
       (eIspProfile == EIspProfile_zHDR_Capture) ||
       (eIspProfile == EIspProfile_MHDR_Preview) ||
       (eIspProfile == EIspProfile_MHDR_Video))
    {   fgProfile = MTRUE;
    }

    if( mbProfileChange != fgProfile){      //Profile changes

       mbProfileChange = fgProfile;

       if (fgProfile){
           if (mAlgoEn){
               mEnv = *((MTK_LCE_ENV_INFO_STRUCT*)&(m_pIspNvramLcePara[1]) );
           }
       }
       else{
           if (mAlgoEn){
               mEnv = *((MTK_LCE_ENV_INFO_STRUCT*)&(m_pIspNvramLcePara[0]) );
           }
       }

       if (mAlgoEn){
           m_pLceAlgo->LceInit(&mEnv, NULL);
           m_pLceAlgo->LceFeatureCtrl(MTKLCE_FEATURE_SET_ENV_INFO, &mEnv, NULL);
       }
    }

    mInInfo.eLceProfile = (eLCE_PROFILE_T)eIspProfile;

    mInInfo.rLCEAEInfo.u4AETarget= rAEInfo.u4AETarget;
    mInInfo.rLCEAEInfo.u4AECurrentTarget= rAEInfo.u4AECurrentTarget;
    mInInfo.rLCEAEInfo.u4Eposuretime= rAEInfo.u4Eposuretime;
    mInInfo.rLCEAEInfo.u4AfeGain= rAEInfo.u4AfeGain;
    mInInfo.rLCEAEInfo.u4IspGain= rAEInfo.u4IspGain;
    mInInfo.rLCEAEInfo.u4RealISOValue= rAEInfo.u4RealISOValue;
    mInInfo.rLCEAEInfo.i4LightValue_x10= rAEInfo.i4LightValue_x10;
    mInInfo.rLCEAEInfo.u4AECondition= rAEInfo.u4AECondition;
    mInInfo.rLCEAEInfo.i2FlareOffset= rAEInfo.i2FlareOffset;
    mInInfo.rLCEAEInfo.i4GammaIdx= rAEInfo.i4GammaIdx;
    mInInfo.rLCEAEInfo.i4LESE_Ratio= rAEInfo.i4LESE_Ratio;
    mInInfo.rLCEAEInfo.u4SWHDR_SE= rAEInfo.u4SWHDR_SE;
    mInInfo.rLCEAEInfo.u4MaxISO= rAEInfo.u4MaxISO;
    mInInfo.rLCEAEInfo.u4AEStableCnt= rAEInfo.u4AEStableCnt;
    mInInfo.rLCEAEInfo.u4OrgExposuretime= rAEInfo.u4OrgExposuretime;
    mInInfo.rLCEAEInfo.u4OrgRealISOValue= rAEInfo.u4OrgRealISOValue;


    for (int i = 0; i<LCE_AE_HISTOGRAM_BIN; i++)
    {
        mInInfo.rLCEAEInfo.u4Histogrm[i]= rAEInfo.u4Histogrm[i];
    }

    mInInfo.rLCEAEInfo.bGammaEnable = rAEInfo.bGammaEnable;

    mInInfo.rLCEAEInfo.i4AEStable = rAEInfo.bAEStable;
    mInInfo.rLCEAEInfo.i4EVRatio = rAEInfo.u4EVRatio; //1024; //FIXME: 1024


    if (mLogEn) printInfo();

    if (mAlgoEn) m_pLceAlgo->LceFeatureCtrl(MTKLCE_FEATURE_SET_PROC_INFO, &mInInfo, NULL);
    if (mAlgoEn) m_pLceAlgo->LceMain();
    if (mAlgoEn) m_pLceAlgo->LceFeatureCtrl(MTKLCE_FEATURE_GET_RESULT, NULL, &mOutLCE);
    if (mAlgoEn) m_pLceAlgo->LceFeatureCtrl(MTKLCE_FEATURE_GET_EXIF, NULL, &mOutExif);

    if (mLogEn)
    {
        MY_LOG_IF(1, "[%s()] m_eSensorDev(%d) after LceMain()", __FUNCTION__, m_eSensorDev);
        printInfo();
        printAEInfo(rAEInfo, "mAEInfo_afterAlgo");
    }

    if (mAlgoEn)
    {

        pLCEReg->qua.bits.LCE_PA = mOutLCE.LCE_A_PA;
        pLCEReg->qua.bits.LCE_PB = mOutLCE.LCE_A_PB;
        pLCEReg->qua.bits.LCE_BA = mOutLCE.LCE_A_BA;

        ::memcpy(pLceExif, &mOutExif, sizeof(NSIspExifDebug::IspExifDebugInfo_T::IspLceInfo));

        if (mLogEn)
        {
            MY_LOG("%s: LCE EXIF: %x/%x/%x/%x/%x; %x/%x/%x/%x/%x; %x/%x/%x/%x/%x; %x/%x/%x/%x/%x; %x/%x/%x/%x/%x; %x/%x"
                , __FUNCTION__

                , mOutExif.i4LceProfile
                , mOutExif.i4LceAutoMode
                , mOutExif.i4ChipVersion
                , mOutExif.i4MainVersion
                , mOutExif.i4SubVersion

                , mOutExif.i4SystemVersion
                , mOutExif.i4LV
                , mOutExif.i4EVRatio
                , mOutExif.u4HistTotal
                , mOutExif.i4ContrastY10

                , mOutExif.i4EVContrastY10
                , mOutExif.i4SegDiv
                , mOutExif.i4ContrastIdx_L
                , mOutExif.i4ContrastIdx_H
                , mOutExif.i4LVIdx_L

                , mOutExif.i4LVIdx_H
                , mOutExif.i4PA
                , mOutExif.i4PB
                , mOutExif.i4BA
                , mOutExif.i4SmoothEnable

                , mOutExif.i4SmoothSpeed
                , mOutExif.i4SmoothWaitAE
                , mOutExif.i4FlareEnable
                , mOutExif.i4FlareOffset
                , mOutExif.i4FixedPA

                , mOutExif.i4FixedPB
                , mOutExif.i4FixedBA
                );




        }
    }
}

MVOID LceMgr::printInfo() const
{

    MY_LOG("%s: mInInfo(%d): %d/%d/%d/%d; %d/%d/%d/%d/%d; %d/%d/%d/%d/%d; %d/%d/%d/%d;"
        , __FUNCTION__

        , mInInfo.rLCEAEInfo.u4AETarget
        , mInInfo.rLCEAEInfo.u4AECurrentTarget
        , mInInfo.rLCEAEInfo.u4Eposuretime
        , mInInfo.rLCEAEInfo.u4AfeGain
        , mInInfo.rLCEAEInfo.u4IspGain

        , mInInfo.rLCEAEInfo.u4RealISOValue
        , mInInfo.rLCEAEInfo.i4LightValue_x10
        , mInInfo.rLCEAEInfo.u4AECondition
        , mInInfo.rLCEAEInfo.i2FlareOffset
        , mInInfo.rLCEAEInfo.i4GammaIdx

        , mInInfo.rLCEAEInfo.i4LESE_Ratio
        , mInInfo.rLCEAEInfo.u4SWHDR_SE
        , mInInfo.rLCEAEInfo.u4MaxISO
        , mInInfo.rLCEAEInfo.u4AEStableCnt
        , mInInfo.rLCEAEInfo.u4OrgExposuretime

        , mInInfo.rLCEAEInfo.u4OrgRealISOValue
        , mInInfo.rLCEAEInfo.bGammaEnable
        , mInInfo.rLCEAEInfo.i4AEStable
        , mInInfo.rLCEAEInfo.i4EVRatio

        );

    /*
        typedef struct {
            MUINT32 u4AETarget;
            MUINT32 u4AECurrentTarget;
            MUINT32 u4Eposuretime;   //!<: Exposure time in ms
            MUINT32 u4AfeGain;           //!<: raw gain
            MUINT32 u4IspGain;           //!<: sensor gain
            MUINT32 u4RealISOValue;
            MINT32  i4LightValue_x10;
            MUINT32 u4AECondition;
            MINT16  i2FlareOffset;
            MINT32  i4GammaIdx;   // next gamma idx
            MINT32  i4LESE_Ratio;    // LE/SE ratio
            MUINT32 u4SWHDR_SE;      //for sw HDR SE ,  -x EV , compare with converge AE
            MUINT32 u4MaxISO;
            MUINT32 u4AEStableCnt;
            MUINT32 u4OrgExposuretime;   //!<: Exposure time in ms
            MUINT32 u4OrgRealISOValue;
            MUINT16 u2Histogrm[GMA_AE_HISTOGRAM_BIN];
            MBOOL bGammaEnable;

            MINT32 i4AEStable;
            MINT32 i4EVRatio;
        } GMA_AE_INFO_T;
    */

    const MUINT32* pHist = &mInInfo.rLCEAEInfo.u4Histogrm[0];
    for (int i=0; i<120; i+=10)
    {
        MY_LOG( "%s: mInInfo.rLCEAEInfo.u4Histogrm[%d-%d]=(%x,%x,%x,%x,%x,  %x,%x,%x,%x,%x)"
            , __FUNCTION__
            , i
            , i+9
            , pHist[i], pHist[i+1], pHist[i+2], pHist[i+3], pHist[i+4]
            , pHist[i+5], pHist[i+6], pHist[i+7], pHist[i+8], pHist[i+9]);
    }
    //(i == 120)
    {
        int i=120;
        MY_LOG( "%s: mInInfo.rLCEAEInfo.u4Histogrm[%d-%d]=(%x,%x,%x,%x,%x,  %x,%x,%x)"
            , __FUNCTION__
            , i
            , i+7
            , pHist[i], pHist[i+1], pHist[i+2], pHist[i+3], pHist[i+4]
            , pHist[i+5], pHist[i+6], pHist[i+7]);
    }


#if 0
    for (int i=0; i<140; i+=10)
    {
        MY_LOG( "%s: mInInfo.i4CurrEncGMA[%d-%d]=(%x,%x,%x,%x,%x,  %x,%x,%x,%x,%x)"
            , __FUNCTION__
            , i
            , i+9
            , mInInfo.i4CurrEncGMA[i], mInInfo.i4CurrEncGMA[i+1], mInInfo.i4CurrEncGMA[i+2], mInInfo.i4CurrEncGMA[i+3], mInInfo.i4CurrEncGMA[i+4]
            , mInInfo.i4CurrEncGMA[i+5], mInInfo.i4CurrEncGMA[i+6], mInInfo.i4CurrEncGMA[i+7], mInInfo.i4CurrEncGMA[i+8], mInInfo.i4CurrEncGMA[i+9]);
    }
    //(i == 140)
    {
        MY_LOG( "%s: mInInfo.i4CurrEncGMA[%d-%d]=(%x,%x,%x,%x)"
            , __FUNCTION__
            , 140
            , 140+3
            , mInInfo.i4CurrEncGMA[140], mInInfo.i4CurrEncGMA[140+1], mInInfo.i4CurrEncGMA[140+2], mInInfo.i4CurrEncGMA[140+3]);

    }
#endif
}


}
