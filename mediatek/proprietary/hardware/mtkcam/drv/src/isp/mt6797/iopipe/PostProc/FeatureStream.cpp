/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#define LOG_TAG "Iop/P2FStm"

#include "FeatureStream.h"
#include "PortMap.h"
#include <IPipe.h>
#include <IPostProcPipe.h>
#include <ispio_sw_scenario.h>
#include <vector>
#include <cutils/atomic.h>

/*************************************************************************************
* Log Utility
*************************************************************************************/
#ifndef USING_MTK_LDVT   // Not using LDVT.
#define  DBG_LOG_TAG        ""
#else
#define  DBG_LOG_TAG        LOG_TAG
#endif


#include <imageio_log.h>    // Note: DBG_LOG_TAG/LEVEL will be used in header file, so header must be included after definition.
DECLARE_DBG_LOG_VARIABLE(P2FeatureStream);
// Clear previous define, use our own define.
#undef LOG_VRB
#undef LOG_DBG
#undef LOG_INF
#undef LOG_WRN
#undef LOG_ERR
#undef LOG_AST
#define LOG_VRB(fmt, arg...)        do { if (P2FeatureStream_DbgLogEnable_VERBOSE) { BASE_LOG_VRB(fmt, ##arg); } } while(0)
#define LOG_DBG(fmt, arg...)        do { if (P2FeatureStream_DbgLogEnable_DEBUG  ) { BASE_LOG_DBG(fmt, ##arg); } } while(0)
#define LOG_INF(fmt, arg...)        do { if (P2FeatureStream_DbgLogEnable_INFO   ) { BASE_LOG_INF(fmt, ##arg); } } while(0)
#define LOG_WRN(fmt, arg...)        do { if (P2FeatureStream_DbgLogEnable_WARN   ) { BASE_LOG_WRN(fmt, ##arg); } } while(0)
#define LOG_ERR(fmt, arg...)        do { if (P2FeatureStream_DbgLogEnable_ERROR  ) { BASE_LOG_ERR(fmt, ##arg); } } while(0)
#define LOG_AST(cond, fmt, arg...)  do { if (P2FeatureStream_DbgLogEnable_ASSERT ) { BASE_LOG_AST(cond, fmt, ##arg); } } while(0)



#define FUNCTION_LOG_START      LOG_INF("+");
#define FUNCTION_LOG_END        LOG_INF("-");
#define ERROR_LOG               LOG_ERR("Error");
//

using namespace NSCam;
using namespace NSIoPipe;
using namespace NSPostProc;

/*
ESoftwareScenario const gSenDevSwScenMap[EFeatureStreamTag_total][SENSOR_DEV_MAIN_3D+1]=
{
    {eSoftwareScenario_Main_Normal_Stream,eSoftwareScenario_Main_Normal_Stream,eSoftwareScenario_Sub_Normal_Stream,\
        eSoftwareScenario_total_num,eSoftwareScenario_total_num,eSoftwareScenario_total_num},//EFeatureStreamTag_Stream
    {eSoftwareScenario_Main_Vfb_Stream_1,eSoftwareScenario_Main_Vfb_Stream_1,eSoftwareScenario_Sub_Vfb_Stream_1,\
        eSoftwareScenario_total_num,eSoftwareScenario_total_num,eSoftwareScenario_total_num},//EFeatureStreamTag_vFB_Stream
    {eSoftwareScenario_Main_Vfb_Stream_2,eSoftwareScenario_Main_Vfb_Stream_2,eSoftwareScenario_Sub_Vfb_Stream_2,\
        eSoftwareScenario_total_num,eSoftwareScenario_total_num,eSoftwareScenario_total_num},//EFeatureStreamTag_vFB_FB
    {eSoftwareScenario_Main_Mfb_Capture,eSoftwareScenario_Main_Mfb_Capture,eSoftwareScenario_Sub_Mfb_Capture,\
        eSoftwareScenario_total_num,eSoftwareScenario_total_num,eSoftwareScenario_total_num},//EFeatureStreamTag_MFB_Cap
    {eSoftwareScenario_Main_Mfb_Blending,eSoftwareScenario_Main_Mfb_Blending,eSoftwareScenario_Sub_Mfb_Blending,\
        eSoftwareScenario_total_num,eSoftwareScenario_total_num,eSoftwareScenario_total_num},//EFeatureStreamTag_MFB_Bld
    {eSoftwareScenario_Main_Mfb_Mixing,eSoftwareScenario_Main_Mfb_Mixing,eSoftwareScenario_Sub_Mfb_Mixing,\
        eSoftwareScenario_total_num,eSoftwareScenario_total_num,eSoftwareScenario_total_num},//EFeatureStreamTag_MFB_Mix
    {eSoftwareScenario_total_num,eSoftwareScenario_Main_Normal_Stream,eSoftwareScenario_total_num,\
        eSoftwareScenario_total_num,eSoftwareScenario_total_num,eSoftwareScenario_total_num},//EFeatureStreamTag_N3D_Stream_M
    {eSoftwareScenario_total_num,eSoftwareScenario_total_num,eSoftwareScenario_total_num,\
        eSoftwareScenario_total_num,eSoftwareScenario_Main2_N3D_Stream,eSoftwareScenario_total_num},//EFeatureStreamTag_N3D_Stream_M2
    {eSoftwareScenario_Main_Pure_Raw_Stream,eSoftwareScenario_Main_Pure_Raw_Stream,eSoftwareScenario_Sub_Pure_Raw_Stream,\
        eSoftwareScenario_total_num,eSoftwareScenario_total_num,eSoftwareScenario_total_num},//EFeatureStreamTag_PRaw_Stream
    {eSoftwareScenario_Main_CC_Raw_Stream,eSoftwareScenario_Main_CC_Raw_Stream,eSoftwareScenario_Sub_CC_Raw_Stream,\
        eSoftwareScenario_total_num,eSoftwareScenario_total_num,eSoftwareScenario_total_num}//EFeatureStreamTag_PRaw_Stream

};
*/

/******************************************************************************
 *
 ******************************************************************************/
 IFeatureStream*
IFeatureStream::
createInstance(
    MUINT32 openedSensorIndex)
{
    FUNCTION_LOG_START;
    //[1] create HalPipeWrapper
    LOG_ERR("plz user NormalStream to do pass2 operation");
    //return new FeatureStream(openedSensorIndex);
    return NULL;
    FUNCTION_LOG_END;
}

//fadeout, remove later
IFeatureStream*
IFeatureStream::
createInstance(
    char const* szCallerName,
    EFeatureStreamTag streamTag,
    MUINT32 openedSensorIndex,
    MBOOL isV3)
{
    LOG_ERR("plz use createInstance(MUINT32 openedSensorIndex)");
    LOG_ERR("plz user NormalStream to do pass2 operation");
    return NULL;
    //FUNCTION_LOG_START;
    //[1] create HalPipeWrapper
    //return new FeatureStream(openedSensorIndex,isV3);
    //FUNCTION_LOG_END;
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
FeatureStream::
destroyInstance()
{
    FUNCTION_LOG_START;
    LOG_ERR("plz user NormalStream to do pass2 operation");
    #if 0
    if(mpHalPipeWrapper==NULL)
    {
        LOG_INF("pipeID(%d),NULL mpHalPipeWrapper.",mPipeID);
    }
    else
    {
        mpHalPipeWrapper->destroyInstance();
        mpHalPipeWrapper=NULL;
    }
    #endif
    FUNCTION_LOG_END;
    delete this;
}
/*******************************************************************************
*
********************************************************************************/
FeatureStream::
FeatureStream(
    MUINT32 openedSensorIndex)    //change to use sensor index, 0xFFFF means pure pass2
            : mpHalPipeWrapper(NULL)
            , mOpenedSensor(SENSOR_DEV_NONE)
            , mHalSensorList(NULL)
{
   //
   LOG_ERR("plz user NormalStream to do pass2 operation");
   #if 0
   mpHalPipeWrapper=HalPipeWrapper::createInstance();
   if(openedSensorIndex==0xFFFF)
   {    //pure pass2
        mOpenedSensor=SENSOR_DEV_NONE;
   }
   else
   {    //p1+p2, get sensor dev firstly
       #ifndef USING_MTK_LDVT
       mHalSensorList=IHalSensorList::get();
       if(mHalSensorList)
       {
           //[1] get sensor dev index from sensorID
            //get sensor output pix id  
            SensorStaticInfo SensorStaticInfo;		   
            MINT32 sensorDEVIdx=mHalSensorList->querySensorDevIdx(openedSensorIndex);
            mOpenedSensor=static_cast<MUINT32>(sensorDEVIdx);
            mHalSensorList->querySensorStaticInfo(mOpenedSensor, &SensorStaticInfo);
            mP2PixId = SensorStaticInfo.sensorFormatOrder;
            LOG_INF("query from sensor openedSensorIndex (%d), eRawPxlID(%d)",mOpenedSensor,mP2PixId);			 
       }
       else
       {
            LOG_ERR("NULL mHalSensorList,pipeID/sidx/sdev(%d/0x%x/0x%x)",mPipeID,openedSensorIndex,mOpenedSensor);
       }
       #else
       //default use main sensor in ldvt load is user pass in sensor index
       mOpenedSensor=SENSOR_DEV_MAIN;
       mP2PixId=0x0;
       LOG_INF("in LDVT load, sdev(0x%x)",mOpenedSensor);
       #endif
   }
   LOG_INF("pipeID/sidx/sdev(%d/0x%x/0x%x),swPipe cID(0x%x)",mPipeID,openedSensorIndex,mOpenedSensor,(&mDequeuedBufList));
   #endif
}


/*******************************************************************************
*
********************************************************************************/
FeatureStream::~FeatureStream()
{

}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
FeatureStream::
init(char const* szCallerName)
{
    FUNCTION_LOG_START;
    LOG_ERR("plz user NormalStream to do pass2 operation");
    #if 0
    Mutex::Autolock autoLock(mModuleMtx);
    MBOOL ret = MTRUE;
    MUINT32 cropPathNum=0;

    //[1] init HalPipeWrapper
    if(!szCallerName)
    {
        LOG_ERR("plz add userName");
        return MFALSE;
    }
        
    LOG_INF("sz/pipeID/openedSensor/pixId (%s/%d/0x%x/0x%x)",szCallerName,mPipeID,mOpenedSensor,mP2PixId);
    if(mpHalPipeWrapper)
    {
        ret=mpHalPipeWrapper->init(szCallerName, mPipeID,mP2PixId);
        if(!ret)
        {
            LOG_ERR("mpHalPipeWrapper init fail, sz/pipeID (%s/%d)",szCallerName,mPipeID);
            return ret;
        }
    }
    else
    {
        LOG_ERR("NULL mpHalPipeWrapper, sz/pipeID (%s/%d)",szCallerName,mPipeID);
        return MFALSE;
    }

    FUNCTION_LOG_END;
    #endif
    return MFALSE;
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
FeatureStream::
uninit(char const* szCallerName)
{
    FUNCTION_LOG_START;
    LOG_ERR("plz user NormalStream to do pass2 operation");
    #if 0
    Mutex::Autolock autoLock(mModuleMtx);
    MBOOL ret = MTRUE;

    //[1] uninit HalPipeWrapper
    LOG_INF("sz/pipeID/openedSensor/pixId (%s/%d/0x%x/0x%x)",szCallerName,mPipeID,mOpenedSensor,mP2PixId);
    if(mpHalPipeWrapper)
    {
        ret=mpHalPipeWrapper->uninit(szCallerName, mPipeID);
        if(!ret)
        {
            LOG_ERR("mpHalPipeWrapper init fail, sz/pipeID (%s/%d)",szCallerName,mPipeID);
            return ret;
        }
    }
    else
    {
        LOG_ERR("NULL mpHalPipeWrapper, sz/pipeID (%s/%d)",szCallerName,mPipeID);
        return MFALSE;
    }
    
    FUNCTION_LOG_END;
    #endif
    return MFALSE;
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
FeatureStream::
enque(
    QParams const& rParams)
{
    FUNCTION_LOG_START;
    LOG_ERR("plz user NormalStream to do pass2 operation");
    #if 0
    Mutex::Autolock autoLock(mModuleMtx);
    bool ret = true;

    if(mpHalPipeWrapper)
    {
        ret=mpHalPipeWrapper->enque(mPipeID, rParams, this->mDequeuedBufList);
        if(!ret)
        {
            LOG_ERR("enque Fail");
        }
    }
    else
    {
        LOG_ERR("NULL mpHalPipeWrapper");
        return MFALSE;
    }

    //FUNCTION_LOG_END;
    #endif
    return MFALSE;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
FeatureStream::
deque(
    QParams& rParams,
    MINT64 i8TimeoutNs)
{
    FUNCTION_LOG_START;
    LOG_ERR("plz user NormalStream to do pass2 operation");
    #if 0
    Mutex::Autolock autoLock(mModuleMtx);
    MBOOL ret = MFALSE;

    if(i8TimeoutNs == -1)   //temp solution for infinite wait
    {
        LOG_INF("no timeout set, infinite wait");
        i8TimeoutNs=8000000000;
    }

    LOG_DBG("pipeID(%d),i8TimeoutNs(%lld)",mPipeID,i8TimeoutNs);
    if(mpHalPipeWrapper)
    {
        ret=mpHalPipeWrapper->deque(rParams, (MUINTPTR)(&mDequeuedBufList), i8TimeoutNs);
        if(!ret)
        {
            LOG_ERR("pipeID(%d/0x%x),i8TimeoutNs(%lld) deque Fail",mPipeID, (MUINTPTR)(&mDequeuedBufList), i8TimeoutNs);
        }
    }
    else
    {
        LOG_ERR("NULL mpHalPipeWrapper");
        ret = MFALSE;
    }
    
    FUNCTION_LOG_END;
    #endif
    return MFALSE;
}

/******************************************************************************
*
******************************************************************************/
MBOOL
FeatureStream::
startVideoRecord(
	MINT32 wd,
	MINT32 ht,
	MINT32 fps)
{
	FUNCTION_LOG_START;
            LOG_ERR("plz user NormalStream to do pass2 operation");
           #if 0
	LOG_INF("mSWScen(%d),mpHalPipeWrapper()0x%x",mSWScen,mpHalPipeWrapper);
	bool ret=true;
	//
	if(mSWScen==eSoftwareScenario_total_num)
	{
		LOG_ERR("plz do init first");
		ret=false;
	}
	else
	{
		if(mpHalPipeWrapper)
		{
			ret=mpHalPipeWrapper->startVideoRecord(wd,ht,fps,mSWScen);
		}
		else
		{
			LOG_ERR("Null pointer");
			ret=false;
		}
	}
	FUNCTION_LOG_END;
    #endif
	return MFALSE;
}

/******************************************************************************
*
******************************************************************************/
MBOOL
FeatureStream::
stopVideoRecord()
{
	FUNCTION_LOG_START;
    LOG_ERR("plz user NormalStream to do pass2 operation");
    #if 0
	bool ret=true;
	//
	if(mSWScen==eSoftwareScenario_total_num)
	{
		LOG_ERR("plz do init first");
		ret=false;
	}
	else
	{
		if(mpHalPipeWrapper)
		{
			ret=mpHalPipeWrapper->stopVideoRecord(mSWScen);
		}
		else
		{
			LOG_ERR("Null pointer");
			ret=false;
		}
	}
	FUNCTION_LOG_END;
    #endif
	return MFALSE;
}

/******************************************************************************
*
******************************************************************************/
MBOOL
FeatureStream::
sendCommand(
    MINT32 cmd,
    MINTPTR arg1,
    MINTPTR arg2)
{
    LOG_ERR("plz user NormalStream to do pass2 operation");
    #if 0
    bool ret=true;

    ret=mpHalPipeWrapper->sendCommand(cmd, mSWScen, arg1, arg2);
    if(!ret)
    {
        LOG_ERR("[Error]sendCommand(0x%x,0x%x,0x%x) Fail",cmd,arg1,arg2);
    }
    #endif
    return MFALSE;
}

/******************************************************************************
*
******************************************************************************/
MBOOL
FeatureStream::
setFps(
    MINT32 fps)
{
    LOG_ERR("plz user NormalStream to do pass2 operation");
    #if 0
    Mutex::Autolock lock(mModuleMtx);

    if(mpHalPipeWrapper)
    {
        mpHalPipeWrapper->setFps(fps);
    }
    else
    {
        LOG_ERR("NULL mpHalPipeWrapper");
        return MFALSE;
    }
    #endif
    return MFALSE;
}

