/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#include <utils/Errors.h>
#include <utils/Mutex.h>    // For android::Mutex.
#include <cutils/log.h>
#include <cutils/properties.h>  // For property_get().
#include <fcntl.h>
#include <sys/mman.h>
#include <linux/mman-proprietary.h>
#include <cutils/atomic.h>

#include "camera_dpe.h"
#include <common/include/common.h>
#include <drv/dpe_drv.h>
#include <drv/dpeunittest.h>


#undef   DBG_LOG_TAG                        // Decide a Log TAG for current file.
#define  DBG_LOG_TAG        "{DpeDrv} "
#include "drv_log.h"                        // Note: DBG_LOG_TAG/LEVEL will be used in header file, so header must be included after definition.
DECLARE_DBG_LOG_VARIABLE(dpe_drv);


// Clear previous define, use our own define.
#undef LOG_VRB
#undef LOG_DBG
#undef LOG_INF
#undef LOG_WRN
#undef LOG_ERR
#undef LOG_AST
#define LOG_VRB(fmt, arg...)        do { if (dpe_drv_DbgLogEnable_VERBOSE) { BASE_LOG_VRB(fmt, ##arg); } } while(0)
#define LOG_DBG(fmt, arg...)        do { if (dpe_drv_DbgLogEnable_DEBUG  ) { BASE_LOG_DBG(fmt, ##arg); } } while(0)
#define LOG_INF(fmt, arg...)        do { if (dpe_drv_DbgLogEnable_INFO   ) { BASE_LOG_INF(fmt, ##arg); } } while(0)
#define LOG_WRN(fmt, arg...)        do { if (dpe_drv_DbgLogEnable_WARN   ) { BASE_LOG_WRN(fmt, ##arg); } } while(0)
#define LOG_ERR(fmt, arg...)        do { if (dpe_drv_DbgLogEnable_ERROR  ) { BASE_LOG_ERR(fmt, ##arg); } } while(0)
#define LOG_AST(cond, fmt, arg...)  do { if (dpe_drv_DbgLogEnable_ASSERT ) { BASE_LOG_AST(cond, fmt, ##arg); } } while(0)

class DpeDbgTimer
{
protected:
    char const*const    mpszName;
    mutable MINT32      mIdx;
    MINT32 const        mi4StartUs;
    mutable MINT32      mi4LastUs;

public:
    DpeDbgTimer(char const*const pszTitle)
        : mpszName(pszTitle)
        , mIdx(0)
        , mi4StartUs(getUs())
        , mi4LastUs(getUs())
    {
    }

    inline MINT32 getUs() const
    {
        struct timeval tv;
        ::gettimeofday(&tv, NULL);
        return tv.tv_sec * 1000000 + tv.tv_usec;
    }

    inline MBOOL ProfilingPrint(char const*const pszInfo = "") const
    {
        MINT32 const i4EndUs = getUs();
        if  (0==mIdx)
        {
            LOG_INF("[%s] %s:(%d-th) ===> [start-->now: %.06f ms]", mpszName, pszInfo, mIdx++, (float)(i4EndUs-mi4StartUs)/1000);
        }
        else
        {
            LOG_INF("[%s] %s:(%d-th) ===> [start-->now: %.06f ms] [last-->now: %.06f ms]", mpszName, pszInfo, mIdx++, (float)(i4EndUs-mi4StartUs)/1000, (float)(i4EndUs-mi4LastUs)/1000);
        }
        mi4LastUs = i4EndUs;

        //sleep(4); //wait 1 sec for AE stable

        return  MTRUE;
    }
};

MINT32              DpeDrv::m_Fd = -1;
volatile MINT32     DpeDrvImp::m_UserCnt = 0;
DPE_DRV_RW_MODE     DpeDrv::m_regRWMode = DPE_DRV_R_ONLY;
MUINT32*            DpeDrvImp::m_pDpeHwRegAddr = NULL;
android::Mutex      DpeDrvImp::m_DpeInitMutex;


char                DpeDrvImp::m_UserName[MAX_USER_NUMBER][32] =
{
    {"\0"},{"\0"},{"\0"},{"\0"},{"\0"},{"\0"},{"\0"},{"\0"},
    {"\0"},{"\0"},{"\0"},{"\0"},{"\0"},{"\0"},{"\0"},{"\0"},
    {"\0"},{"\0"},{"\0"},{"\0"},{"\0"},{"\0"},{"\0"},{"\0"},
    {"\0"},{"\0"},{"\0"},{"\0"},{"\0"},{"\0"},{"\0"},{"\0"},
};

DpeDrvImp::DpeDrvImp()
{
    DBG_LOG_CONFIG(drv, dpe_drv);
    LOG_VRB("getpid[0x%08x],gettid[0x%08x]", getpid() ,gettid());
    m_pDpeHwRegAddr = NULL;
}

DpeDrv* DpeDrv::createInstance()
{
    return DpeDrvImp::getInstance();
}

static DpeDrvImp singleton;

DpeDrv* DpeDrvImp::getInstance()
{
    LOG_DBG("singleton[0x%08x].", &singleton);

    return &singleton;
}


MBOOL DpeDrvImp::init(const char* userName)
{
    MBOOL Result = MTRUE;
    MUINT32 resetModule;
    //
    android::Mutex::Autolock lock(this->m_DpeInitMutex);
    //
    LOG_INF("+,m_UserCnt(%d), curUser(%s).", this->m_UserCnt,userName);
    //
    if( (strlen(userName)<1) || (strlen(userName) >= MAX_USER_NAME_SIZE) || (userName==NULL))
    {
        LOG_ERR("Plz add userName if you want to use dpe driver\n");
        return MFALSE;
    }
    //
    if(this->m_UserCnt > 0)
    {
        if(this->m_UserCnt < MAX_USER_NUMBER){
            strcpy((char*)this->m_UserName[this->m_UserCnt],userName);
            android_atomic_inc(&this->m_UserCnt);
            LOG_INF(" - X. m_UserCnt: %d.", this->m_UserCnt);
            return Result;
        }
        else{
            LOG_ERR("m_userCnt is over upper bound\n");
            return MFALSE;
        }
    }

    // Open dpe device
    this->m_Fd = open(DPE_DRV_DEV_NAME, O_RDWR);
    if (this->m_Fd < 0)    // 1st time open failed.
    {
        LOG_ERR("DPE kernel 1st open fail, errno(%d):%s.", errno, strerror(errno));
        // Try again, using "Read Only".
        this->m_Fd = open(DPE_DRV_DEV_NAME, O_RDONLY);
        if (this->m_Fd < 0) // 2nd time open failed.
        {
            LOG_ERR("DPE kernel 2nd open fail, errno(%d):%s.", errno, strerror(errno));
            Result = MFALSE;
            goto EXIT;
        }
        else
            this->m_regRWMode=DPE_DRV_R_ONLY;
    }
    else    // 1st time open success.   // Sometimes GDMA will go this path, too. e.g. File Manager -> Phone Storage -> Photo.
    {
            // fd opened only once at the very 1st init
            m_pDpeHwRegAddr = (MUINT32 *) mmap(0, DPE_REG_RANGE, (PROT_READ | PROT_WRITE | PROT_NOCACHE), MAP_SHARED, this->m_Fd, DPE_BASE_HW);

            if(m_pDpeHwRegAddr == MAP_FAILED)
            {
                LOG_ERR("DPE mmap fail, errno(%d):%s", errno, strerror(errno));
                Result = MFALSE;
                goto EXIT;
            }
            this->m_regRWMode=DPE_DRV_RW_MMAP;
    }


    if(ioctl(this->m_Fd, DPE_RESET, NULL) < 0){
        LOG_ERR("DPE Reset fail !!\n");
        Result = MFALSE;
        goto EXIT;
    }

    //
    strcpy((char*)this->m_UserName[this->m_UserCnt],userName);
    android_atomic_inc(&this->m_UserCnt);

EXIT:

    //
    if (!Result)    // If some init step goes wrong.
    {
        if(this->m_Fd >= 0)
        {
            // unmap to avoid memory leakage
            munmap(m_pDpeHwRegAddr, DPE_REG_RANGE);

            close(this->m_Fd);
            this->m_Fd = -1;
            LOG_INF("close dpe device Fd");
        }
    }

    LOG_DBG("-,ret: %d. mInitCount:(%d),m_pDpeHwRegAddr(0x%x)", Result, this->m_UserCnt, m_pDpeHwRegAddr);
    return Result;
}

//-----------------------------------------------------------------------------
MBOOL DpeDrvImp::uninit(const char* userName)
{
    MBOOL Result = MTRUE;
    MUINT32 bMatch = 0;
    //
    android::Mutex::Autolock lock(this->m_DpeInitMutex);
    //
    LOG_INF("-,m_UserCnt(%d),curUser(%s)", this->m_UserCnt,userName);
    //
    if( (strlen(userName)<1) || (strlen(userName) >= MAX_USER_NAME_SIZE) || (userName==NULL))
    {
        LOG_ERR("Plz add userName if you want to uninit dpe driver\n");
        return MFALSE;
    }

    //
    if(this->m_UserCnt <= 0)
    {
        LOG_ERR("no more user in DpeDrv , curUser(%s)",userName);
        goto EXIT;
    }

    for(MUINT32 i=0;i<MAX_USER_NUMBER;i++){
        if(strcmp((const char*)this->m_UserName[i],userName) == 0){
            bMatch = i+1;   //avoid match at the very 1st
            break;
        }
    }

    if(!bMatch){
        LOG_ERR("no matching username:%s\n",userName);
        for(MUINT32 i=0;i<MAX_USER_NUMBER;i+=4)
           LOG_ERR("current user: %s, %s, %s, %s\n",this->m_UserName[i],this->m_UserName[i+1],this->m_UserName[i+2],this->m_UserName[i+3]);
        return MFALSE;
    }
    else
        this->m_UserName[bMatch-1][0] = '\0';

    // More than one user
    android_atomic_dec(&this->m_UserCnt);

    if(this->m_UserCnt > 0)    // If there are still users, exit.
        goto EXIT;

    if(m_pDpeHwRegAddr != MAP_FAILED){
         munmap(m_pDpeHwRegAddr, DPE_REG_RANGE);
    }


    //
    if(this->m_Fd >= 0)
    {
        close(this->m_Fd);
        this->m_Fd = -1;
        this->m_regRWMode=DPE_DRV_R_ONLY;
    }

    //
EXIT:

    LOG_INF(" - X. ret: %d. m_UserCnt: %d.", Result, this->m_UserCnt);

    if(this->m_UserCnt!= 0){
        LOG_INF("current working user:\n");
        for(MUINT32 i=0;i<MAX_USER_NUMBER;i+=8)
            LOG_INF("current user: %s, %s, %s, %s, %s, %s, %s, %s\n"    \
            ,this->m_UserName[i],this->m_UserName[i+1],this->m_UserName[i+2],this->m_UserName[i+3]  \
            ,this->m_UserName[i+4],this->m_UserName[i+5],this->m_UserName[i+6],this->m_UserName[i+7]);
    }
    return Result;
}

#define FD_CHK()({\
    MINT32 Ret=0;\
    if(this->m_Fd < 0){\
        LOG_ERR("no dpe device\n");\
        Ret = -1;\
    }\
    Ret;\
})

MBOOL DpeDrvImp::waitIrq(DPE_WAIT_IRQ_STRUCT* pWaitIrq)
{
    MINT32 Ret = 0;
    MUINT32 OrgTimeOut;
    DPE_IRQ_CLEAR_ENUM OrgClr;
    DpeDbgTimer dbgTmr("waitIrq");
    MUINT32 Ta=0,Tb=0;
    DPE_WAIT_IRQ_STRUCT waitirq;
    LOG_DBG(" - E. Status(0x%08x),Timeout(%d).\n",pWaitIrq->Status, pWaitIrq->Timeout);
    if(-1 == FD_CHK()){
        return MFALSE;
    }
    waitirq.Clear=pWaitIrq->Clear;
    waitirq.Type=pWaitIrq->Type;
    waitirq.Status=pWaitIrq->Status;
    waitirq.Timeout=pWaitIrq->Timeout;
    waitirq.UserKey=pWaitIrq->UserKey;
    waitirq.ProcessID=pWaitIrq->ProcessID;
    waitirq.bDumpReg=pWaitIrq->bDumpReg;

    while( waitirq.Timeout > 0 )//receive restart system call again
    {
        Ta=dbgTmr.getUs();
        Ret = ioctl(this->m_Fd,DPE_WAIT_IRQ,&waitirq);
        Tb=dbgTmr.getUs();
        if( Ret== (-SIG_ERESTARTSYS) )
        {
            waitirq.Timeout=waitirq.Timeout - ((Tb-Ta)/1000);
            LOG_INF("ERESTARTSYS,Type(%d),Status(0x%08x),Timeout(%d us)\n",waitirq.Type, waitirq.Status, waitirq.Timeout);
        }
        else
        {
            break;
        }
    }

    if(Ret < 0) {
        LOG_ERR("DPE_WAIT_IRQ fail(%d). Wait Status(0x%08x), Timeout(%d).\n", Ret,  pWaitIrq->Status, pWaitIrq->Timeout);
        return MFALSE;
    }


    return MTRUE;
}

MBOOL DpeDrvImp::clearIrq(DPE_CLEAR_IRQ_STRUCT* pClearIrq)
{
    MINT32 Ret;
    DPE_CLEAR_IRQ_STRUCT clear;
    //
    LOG_DBG(" - E. user(%d), Status(%d)\n",pClearIrq->UserKey, pClearIrq->Status);
    if(-1 == FD_CHK()){
        return MFALSE;
    }

    memcpy(&clear, pClearIrq, sizeof(DPE_CLEAR_IRQ_STRUCT));

    Ret = ioctl(this->m_Fd,DPE_CLEAR_IRQ,&clear);
    if(Ret < 0)
    {
        LOG_ERR("DPE_CLEAR_IRQ fail(%d)\n",Ret);
        return MFALSE;
    }
    return MTRUE;
}



MBOOL DpeDrvImp::waitDPEFrameDone(unsigned int Status, MINT32 timeoutMs)
{
    DPE_WAIT_IRQ_STRUCT WaitIrq;
    WaitIrq.Clear = DPE_IRQ_WAIT_CLEAR;
    WaitIrq.Type = DPE_IRQ_TYPE_INT_DPE_ST;
    WaitIrq.Status = Status;
    WaitIrq.Timeout = timeoutMs;
    WaitIrq.UserKey = 0x0; //Driver Key
    WaitIrq.ProcessID = 0x0;
    WaitIrq.bDumpReg = 0x1;

    DRV_TRACE_BEGIN("DPE waitIrq");

    if (MTRUE == waitIrq(&WaitIrq))
    {
        if (Status & DPE_DVE_INT_ST)
        {
            LOG_DBG("DPE Wait DVE Interupt Frame Done Success!!\n");
        }
        else
        {
            LOG_DBG("DPE Wait WMFE Interupt Frame Done Success!!\n");
        }
        DRV_TRACE_END();
        return MTRUE;
    }
    else
    {
        if (Status & DPE_DVE_INT_ST)
        {
            LOG_DBG("DPE Wait DVE Interupt Frame Done Fail!!\n");
        }
        else
        {
            LOG_DBG("DPE Wait WMFE Interupt Frame Done Fail!!\n");
        }
        DRV_TRACE_END();
        return MFALSE;
    }


}


MBOOL DpeDrvImp::enqueDVE(vector<NSCam::NSIoPipe::DVEConfig>& DveConfigVec)
{
    MINT32 Ret;
    unsigned int num = DveConfigVec.size();
    NSCam::NSIoPipe::DVEConfig* pDveConfig;
    DPE_DVERequest dpe_dveRequest;
    DPE_DVEConfig dpe_DveConfig[_SUPPORT_MAX_DPE_FRAME_REQUEST_];
#if 1


    for (unsigned int i=0; i<DveConfigVec.size(); i++)
    {
        pDveConfig = &(DveConfigVec.at(i));

        if ((DPE_DEFAULT_UT == g_DPE_UnitTest_Num) || (DPE_TESTCASE_UT_0 == g_DPE_UnitTest_Num))
        {
            dpe_DveConfig[i].DPE_DVE_CTRL = (0x3f20000) | (pDveConfig->Dve_Imgi_r_Fmt<<10) | (pDveConfig->Dve_Imgi_l_Fmt<<8) | (pDveConfig->Dve_Mask_En <<6) | (pDveConfig->Dve_r_Bbox_En <<5) | (pDveConfig->Dve_l_Bbox_En <<4) | (pDveConfig->Dve_Skp_Pre_Dv <<3) \
                 | ((pDveConfig->Dve_Vert_Ds_Mode & 0x01)<< 2) | ((pDveConfig->Dve_Horz_Ds_Mode & 0x01)<< 1);
        }
        else
        {
            dpe_DveConfig[i].DPE_DVE_CTRL = (0x3920000) | (pDveConfig->Dve_Imgi_r_Fmt<<10) | (pDveConfig->Dve_Imgi_l_Fmt<<8) | (pDveConfig->Dve_Mask_En <<6) | (pDveConfig->Dve_r_Bbox_En <<5) | (pDveConfig->Dve_l_Bbox_En <<4) | (pDveConfig->Dve_Skp_Pre_Dv <<3) \
                 | ((pDveConfig->Dve_Vert_Ds_Mode & 0x01)<< 2) | ((pDveConfig->Dve_Horz_Ds_Mode & 0x01)<< 1);
        }

        dpe_DveConfig[i].DPE_DVE_ORG_L_HORZ_BBOX = (pDveConfig->Dve_Org_l_Bbox.DVE_ORG_BBOX_RIGHT << 16) | pDveConfig->Dve_Org_l_Bbox.DVE_ORG_BBOX_LEFT;
        dpe_DveConfig[i].DPE_DVE_ORG_L_VERT_BBOX = (pDveConfig->Dve_Org_l_Bbox.DVE_ORG_BBOX_BOTTOM << 16) | pDveConfig->Dve_Org_l_Bbox.DVE_ORG_BBOX_TOP;
        dpe_DveConfig[i].DPE_DVE_ORG_R_HORZ_BBOX = (pDveConfig->Dve_Org_r_Bbox.DVE_ORG_BBOX_RIGHT << 16) | pDveConfig->Dve_Org_r_Bbox.DVE_ORG_BBOX_LEFT;
        dpe_DveConfig[i].DPE_DVE_ORG_R_VERT_BBOX = (pDveConfig->Dve_Org_r_Bbox.DVE_ORG_BBOX_BOTTOM << 16) | pDveConfig->Dve_Org_r_Bbox.DVE_ORG_BBOX_TOP;
        dpe_DveConfig[i].DPE_DVE_ORG_SIZE = (pDveConfig->Dve_Org_Height <<16) | pDveConfig->Dve_Org_Width;
        dpe_DveConfig[i].DPE_DVE_ORG_SR_0 = (pDveConfig->Dve_Org_Horz_Sr_1 <<16) | pDveConfig->Dve_Org_Horz_Sr_0;
        dpe_DveConfig[i].DPE_DVE_ORG_SR_1 = pDveConfig->Dve_Org_Vert_Sr_0;
        dpe_DveConfig[i].DPE_DVE_ORG_SV = (pDveConfig->Dve_Org_Start_Vert_Sv <<16) | pDveConfig->Dve_Org_Start_Horz_Sv;
        dpe_DveConfig[i].DPE_DVE_CAND_NUM = pDveConfig->Dve_Cand_Num;
        dpe_DveConfig[i].DPE_DVE_CAND_SEL_0 = (pDveConfig->Dve_Cand_3.DVE_CAND_SEL << 24) | (pDveConfig->Dve_Cand_2.DVE_CAND_SEL << 16) | (pDveConfig->Dve_Cand_1.DVE_CAND_SEL << 8) | (pDveConfig->Dve_Cand_0.DVE_CAND_SEL);
        dpe_DveConfig[i].DPE_DVE_CAND_SEL_1 = (pDveConfig->Dve_Cand_7.DVE_CAND_SEL << 24) | (pDveConfig->Dve_Cand_6.DVE_CAND_SEL << 16) | (pDveConfig->Dve_Cand_5.DVE_CAND_SEL << 8) | (pDveConfig->Dve_Cand_4.DVE_CAND_SEL);
        dpe_DveConfig[i].DPE_DVE_CAND_SEL_2 = 0x10081206;
        dpe_DveConfig[i].DPE_DVE_CAND_TYPE_0 = (pDveConfig->Dve_Cand_7.DVE_CAND_TYPE << 28) | (pDveConfig->Dve_Cand_6.DVE_CAND_TYPE << 24) | (pDveConfig->Dve_Cand_5.DVE_CAND_TYPE << 20) | (pDveConfig->Dve_Cand_4.DVE_CAND_TYPE << 16) | (pDveConfig->Dve_Cand_3.DVE_CAND_TYPE << 12) | (pDveConfig->Dve_Cand_2.DVE_CAND_TYPE << 8) | (pDveConfig->Dve_Cand_1.DVE_CAND_TYPE << 4) | (pDveConfig->Dve_Cand_0.DVE_CAND_TYPE);
        dpe_DveConfig[i].DPE_DVE_CAND_TYPE_1 = 0x2121;
        dpe_DveConfig[i].DPE_DVE_RAND_LUT = (pDveConfig->Dve_Rand_Lut_3 << 24) | (pDveConfig->Dve_Rand_Lut_2 << 16) | (pDveConfig->Dve_Rand_Lut_1 << 8) | (pDveConfig->Dve_Rand_Lut_0);
        dpe_DveConfig[i].DPE_DVE_GMV = (pDveConfig->DVE_VERT_GMV << 16) | pDveConfig->DVE_HORZ_GMV;
        dpe_DveConfig[i].DPE_DVE_DV_INI = pDveConfig->Dve_Horz_Dv_Ini;
        dpe_DveConfig[i].DPE_DVE_BLK_VAR_CTRL = (pDveConfig->Dve_Coft_Shift << 16) | pDveConfig->Dve_Corner_Th;
        dpe_DveConfig[i].DPE_DVE_SMTH_LUMA_CTRL =  (pDveConfig->Dve_Smth_Luma_Th_1 << 24) | (pDveConfig->Dve_Smth_Luma_Th_0 << 16) | (pDveConfig->Dve_Smth_Luma_Ada_Base << 8) | pDveConfig->Dve_Smth_Luma_Horz_Pnlty_Sel;
        dpe_DveConfig[i].DPE_DVE_SMTH_DV_CTRL = (pDveConfig->Dve_Smth_Dv_Th_1 << 28) | (pDveConfig->Dve_Smth_Dv_Th_0 << 20) | (pDveConfig->Dve_Smth_Dv_Ada_Base << 12) | (pDveConfig->Dve_Smth_Dv_Vert_Pnlty_Sel << 8) | (pDveConfig->Dve_Smth_Dv_Horz_Pnlty_Sel << 4) | pDveConfig->Dve_Smth_Dv_Mode;
        dpe_DveConfig[i].DPE_DVE_ORD_CTRL =  (pDveConfig->Dve_Ord_Th << 16) | (pDveConfig->Dve_Ord_Coring << 4) | pDveConfig->Dve_Ord_Pnlty_Sel;
        dpe_DveConfig[i].DPE_DVE_TYPE_CTRL_0 =  (pDveConfig->Dve_Type_Penality_Ctrl.DVE_REFINE_PNLTY_SEL << 28) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_GMV_PNLTY_SEL << 24) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_PREV_PNLTY_SEL << 20) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_NBR_PNLTY_SEL << 16) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_RAND_PNLTY_SEL << 8) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_TMPR_PNLTY_SEL << 4) | pDveConfig->Dve_Type_Penality_Ctrl.DVE_SPTL_PNLTY_SEL;
        dpe_DveConfig[i].DPE_DVE_TYPE_CTRL_1 =  (pDveConfig->Dve_Type_Penality_Ctrl.DVE_RAND_COST << 24) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_GMV_COST << 20) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_PREV_COST << 16) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_NBR_COST << 12) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_REFINE_COST << 8) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_TMPR_COST << 4) | pDveConfig->Dve_Type_Penality_Ctrl.DVE_SPTL_COST;


        dpe_DveConfig[i].DPE_DVE_IMGI_L_BASE_ADDR =  pDveConfig->Dve_Imgi_l.u4BufPA;
        dpe_DveConfig[i].DPE_DVE_IMGI_L_STRIDE =  pDveConfig->Dve_Imgi_l.u4Stride;
        dpe_DveConfig[i].DPE_DVE_IMGI_R_BASE_ADDR =  pDveConfig->Dve_Imgi_r.u4BufPA;
        dpe_DveConfig[i].DPE_DVE_IMGI_R_STRIDE =  pDveConfig->Dve_Imgi_r.u4Stride;
        dpe_DveConfig[i].DPE_DVE_DVI_L_BASE_ADDR =  pDveConfig->Dve_Dvi_l.u4BufPA;
        dpe_DveConfig[i].DPE_DVE_DVI_L_STRIDE =  pDveConfig->Dve_Dvi_l.u4Stride;
        dpe_DveConfig[i].DPE_DVE_DVI_R_BASE_ADDR =  pDveConfig->Dve_Dvi_r.u4BufPA;
        dpe_DveConfig[i].DPE_DVE_DVI_R_STRIDE =  pDveConfig->Dve_Dvi_r.u4Stride;
        dpe_DveConfig[i].DPE_DVE_MASKI_L_BASE_ADDR =  pDveConfig->Dve_Maski_l.u4BufPA;
        dpe_DveConfig[i].DPE_DVE_MASKI_L_STRIDE =  pDveConfig->Dve_Maski_l.u4Stride;
        dpe_DveConfig[i].DPE_DVE_MASKI_R_BASE_ADDR =  pDveConfig->Dve_Maski_r.u4BufPA;
        dpe_DveConfig[i].DPE_DVE_MASKI_R_STRIDE =  pDveConfig->Dve_Maski_r.u4Stride;
        dpe_DveConfig[i].DPE_DVE_DVO_L_BASE_ADDR =  pDveConfig->Dve_Dvo_l.u4BufPA;
        dpe_DveConfig[i].DPE_DVE_DVO_L_STRIDE =  pDveConfig->Dve_Dvo_l.u4Stride;
        dpe_DveConfig[i].DPE_DVE_DVO_R_BASE_ADDR =  pDveConfig->Dve_Dvo_r.u4BufPA;
        dpe_DveConfig[i].DPE_DVE_DVO_R_STRIDE =  pDveConfig->Dve_Dvo_r.u4Stride;
        dpe_DveConfig[i].DPE_DVE_CONFO_L_BASE_ADDR =  pDveConfig->Dve_Confo_l.u4BufPA;
        dpe_DveConfig[i].DPE_DVE_CONFO_L_STRIDE =  pDveConfig->Dve_Confo_l.u4Stride;
        dpe_DveConfig[i].DPE_DVE_CONFO_R_BASE_ADDR =  pDveConfig->Dve_Confo_r.u4BufPA;
        dpe_DveConfig[i].DPE_DVE_CONFO_R_STRIDE =  pDveConfig->Dve_Confo_r.u4Stride;
        dpe_DveConfig[i].DPE_DVE_RESPO_L_BASE_ADDR =  pDveConfig->Dve_Respo_l.u4BufPA;
        dpe_DveConfig[i].DPE_DVE_RESPO_L_STRIDE =  pDveConfig->Dve_Respo_l.u4Stride;
        dpe_DveConfig[i].DPE_DVE_RESPO_R_BASE_ADDR =  pDveConfig->Dve_Respo_r.u4BufPA;
        dpe_DveConfig[i].DPE_DVE_RESPO_R_STRIDE =  pDveConfig->Dve_Respo_r.u4Stride;


        LOG_DBG("DPE_DVE_IMGI_L_BASE_ADDR:(0x%x)\n", dpe_DveConfig[i].DPE_DVE_IMGI_L_BASE_ADDR);
        LOG_DBG("DPE_DVE_IMGI_R_BASE_ADDR:(0x%x)\n", dpe_DveConfig[i].DPE_DVE_IMGI_R_BASE_ADDR);
        LOG_DBG("DPE_DVE_DVI_L_BASE_ADDR:(0x%x)\n", dpe_DveConfig[i].DPE_DVE_DVI_L_BASE_ADDR);
        LOG_DBG("DPE_DVE_DVI_R_BASE_ADDR:(0x%x)\n", dpe_DveConfig[i].DPE_DVE_DVI_R_BASE_ADDR);
        LOG_DBG("DPE_DVE_MASKI_L_BASE_ADDR:(0x%x)\n", dpe_DveConfig[i].DPE_DVE_MASKI_L_BASE_ADDR);
        LOG_DBG("DPE_DVE_MASKI_R_BASE_ADDR:(0x%x)\n", dpe_DveConfig[i].DPE_DVE_MASKI_R_BASE_ADDR);
        LOG_DBG("DPE_DVE_DVO_L_BASE_ADDR:(0x%x)\n", dpe_DveConfig[i].DPE_DVE_DVO_L_BASE_ADDR);
        LOG_DBG("DPE_DVE_DVO_R_BASE_ADDR:(0x%x)\n", dpe_DveConfig[i].DPE_DVE_DVO_R_BASE_ADDR);
        LOG_DBG("DPE_DVE_CONFO_L_BASE_ADDR:(0x%x)\n", dpe_DveConfig[i].DPE_DVE_CONFO_L_BASE_ADDR);
        LOG_DBG("DPE_DVE_CONFO_R_BASE_ADDR:(0x%x)\n", dpe_DveConfig[i].DPE_DVE_CONFO_R_BASE_ADDR);
        LOG_DBG("DPE_DVE_RESPO_L_BASE_ADDR:(0x%x)\n", dpe_DveConfig[i].DPE_DVE_RESPO_L_BASE_ADDR);
        LOG_DBG("DPE_DVE_RESPO_R_BASE_ADDR:(0x%x)\n", dpe_DveConfig[i].DPE_DVE_RESPO_R_BASE_ADDR);



    }

    dpe_dveRequest.m_ReqNum = num;
    dpe_dveRequest.m_pDpeConfig = dpe_DveConfig;

    Ret = ioctl(this->m_Fd,DPE_DVE_ENQUE_REQ,&dpe_dveRequest);
    if(Ret < 0)
    {
        LOG_ERR("DPE_DVE_ENQUE_REQ fail(%d)\n", Ret);
        return MFALSE;
    }

#else
    MINT32 Ret;
    unsigned int num = DveConfigVec.size();
    NSCam::NSIoPipe::DVEConfig* pDveConfig;
    DPE_DVEConfig dpe_DveConfig;
    Ret = ioctl(this->m_Fd,DPE_DVE_ENQNUE_NUM,&num);
    if(Ret < 0)
    {
        LOG_ERR("DPE_DVE_EQNUE_NUM fail(%d)\n", Ret);
        return MFALSE;
    }

    for (unsigned int i=0; i<DveConfigVec.size(); i++)
    {
        pDveConfig = &(DveConfigVec.at(i));

        if ((DPE_DEFAULT_UT == g_DPE_UnitTest_Num) || (DPE_TESTCASE_UT_0 == g_DPE_UnitTest_Num))
        {
            dpe_DveConfig.DPE_DVE_CTRL = (0x3f20000) | (pDveConfig->Dve_Imgi_r_Fmt<<10) | (pDveConfig->Dve_Imgi_l_Fmt<<8) | (pDveConfig->Dve_Mask_En <<6) | (pDveConfig->Dve_r_Bbox_En <<5) | (pDveConfig->Dve_l_Bbox_En <<4) | (pDveConfig->Dve_Skp_Pre_Dv <<3) \
                 | ((pDveConfig->Dve_Vert_Ds_Mode & 0x01)<< 2) | ((pDveConfig->Dve_Horz_Ds_Mode & 0x01)<< 1);
        }
        else
        {
            dpe_DveConfig.DPE_DVE_CTRL = (0x3920000) | (pDveConfig->Dve_Imgi_r_Fmt<<10) | (pDveConfig->Dve_Imgi_l_Fmt<<8) | (pDveConfig->Dve_Mask_En <<6) | (pDveConfig->Dve_r_Bbox_En <<5) | (pDveConfig->Dve_l_Bbox_En <<4) | (pDveConfig->Dve_Skp_Pre_Dv <<3) \
                 | ((pDveConfig->Dve_Vert_Ds_Mode & 0x01)<< 2) | ((pDveConfig->Dve_Horz_Ds_Mode & 0x01)<< 1);
        }

        dpe_DveConfig.DPE_DVE_ORG_L_HORZ_BBOX = (pDveConfig->Dve_Org_l_Bbox.DVE_ORG_BBOX_RIGHT << 16) | pDveConfig->Dve_Org_l_Bbox.DVE_ORG_BBOX_LEFT;
        dpe_DveConfig.DPE_DVE_ORG_L_VERT_BBOX = (pDveConfig->Dve_Org_l_Bbox.DVE_ORG_BBOX_BOTTOM << 16) | pDveConfig->Dve_Org_l_Bbox.DVE_ORG_BBOX_TOP;
        dpe_DveConfig.DPE_DVE_ORG_R_HORZ_BBOX = (pDveConfig->Dve_Org_r_Bbox.DVE_ORG_BBOX_RIGHT << 16) | pDveConfig->Dve_Org_r_Bbox.DVE_ORG_BBOX_LEFT;
        dpe_DveConfig.DPE_DVE_ORG_R_VERT_BBOX = (pDveConfig->Dve_Org_r_Bbox.DVE_ORG_BBOX_BOTTOM << 16) | pDveConfig->Dve_Org_r_Bbox.DVE_ORG_BBOX_TOP;
        dpe_DveConfig.DPE_DVE_ORG_SIZE = (pDveConfig->Dve_Org_Height <<16) | pDveConfig->Dve_Org_Width;
        dpe_DveConfig.DPE_DVE_ORG_SR_0 = (pDveConfig->Dve_Org_Horz_Sr_1 <<16) | pDveConfig->Dve_Org_Horz_Sr_0;
        dpe_DveConfig.DPE_DVE_ORG_SR_1 = pDveConfig->Dve_Org_Vert_Sr_0;
        dpe_DveConfig.DPE_DVE_ORG_SV = (pDveConfig->Dve_Org_Start_Vert_Sv <<16) | pDveConfig->Dve_Org_Start_Horz_Sv;
        dpe_DveConfig.DPE_DVE_CAND_NUM = pDveConfig->Dve_Cand_Num;
        dpe_DveConfig.DPE_DVE_CAND_SEL_0 = (pDveConfig->Dve_Cand_3.DVE_CAND_SEL << 24) | (pDveConfig->Dve_Cand_2.DVE_CAND_SEL << 16) | (pDveConfig->Dve_Cand_1.DVE_CAND_SEL << 8) | (pDveConfig->Dve_Cand_0.DVE_CAND_SEL);
        dpe_DveConfig.DPE_DVE_CAND_SEL_1 = (pDveConfig->Dve_Cand_7.DVE_CAND_SEL << 24) | (pDveConfig->Dve_Cand_6.DVE_CAND_SEL << 16) | (pDveConfig->Dve_Cand_5.DVE_CAND_SEL << 8) | (pDveConfig->Dve_Cand_4.DVE_CAND_SEL);
        dpe_DveConfig.DPE_DVE_CAND_SEL_2 = 0x10081206;
        dpe_DveConfig.DPE_DVE_CAND_TYPE_0 = (pDveConfig->Dve_Cand_7.DVE_CAND_TYPE << 28) | (pDveConfig->Dve_Cand_6.DVE_CAND_TYPE << 24) | (pDveConfig->Dve_Cand_5.DVE_CAND_TYPE << 20) | (pDveConfig->Dve_Cand_4.DVE_CAND_TYPE << 16) | (pDveConfig->Dve_Cand_3.DVE_CAND_TYPE << 12) | (pDveConfig->Dve_Cand_2.DVE_CAND_TYPE << 8) | (pDveConfig->Dve_Cand_1.DVE_CAND_TYPE << 4) | (pDveConfig->Dve_Cand_0.DVE_CAND_TYPE);
        dpe_DveConfig.DPE_DVE_CAND_TYPE_1 = 0x2121;
        dpe_DveConfig.DPE_DVE_RAND_LUT = (pDveConfig->Dve_Rand_Lut_3 << 24) | (pDveConfig->Dve_Rand_Lut_2 << 16) | (pDveConfig->Dve_Rand_Lut_1 << 8) | (pDveConfig->Dve_Rand_Lut_0);
        dpe_DveConfig.DPE_DVE_GMV = (pDveConfig->DVE_VERT_GMV << 16) | pDveConfig->DVE_HORZ_GMV;
        dpe_DveConfig.DPE_DVE_DV_INI = pDveConfig->Dve_Horz_Dv_Ini;
        dpe_DveConfig.DPE_DVE_BLK_VAR_CTRL = (pDveConfig->Dve_Coft_Shift << 16) | pDveConfig->Dve_Corner_Th;
        dpe_DveConfig.DPE_DVE_SMTH_LUMA_CTRL =  (pDveConfig->Dve_Smth_Luma_Th_1 << 24) | (pDveConfig->Dve_Smth_Luma_Th_0 << 16) | (pDveConfig->Dve_Smth_Luma_Ada_Base << 8) | pDveConfig->Dve_Smth_Luma_Horz_Pnlty_Sel;
        dpe_DveConfig.DPE_DVE_SMTH_DV_CTRL = (pDveConfig->Dve_Smth_Dv_Th_1 << 28) | (pDveConfig->Dve_Smth_Dv_Th_0 << 20) | (pDveConfig->Dve_Smth_Dv_Ada_Base << 12) | (pDveConfig->Dve_Smth_Dv_Vert_Pnlty_Sel << 8) | (pDveConfig->Dve_Smth_Dv_Horz_Pnlty_Sel << 4) | pDveConfig->Dve_Smth_Dv_Mode;
        dpe_DveConfig.DPE_DVE_ORD_CTRL =  (pDveConfig->Dve_Ord_Th << 16) | (pDveConfig->Dve_Ord_Coring << 4) | pDveConfig->Dve_Ord_Pnlty_Sel;
        dpe_DveConfig.DPE_DVE_TYPE_CTRL_0 =  (pDveConfig->Dve_Type_Penality_Ctrl.DVE_REFINE_PNLTY_SEL << 28) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_GMV_PNLTY_SEL << 24) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_PREV_PNLTY_SEL << 20) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_NBR_PNLTY_SEL << 16) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_RAND_PNLTY_SEL << 8) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_TMPR_PNLTY_SEL << 4) | pDveConfig->Dve_Type_Penality_Ctrl.DVE_SPTL_PNLTY_SEL;
        dpe_DveConfig.DPE_DVE_TYPE_CTRL_1 =  (pDveConfig->Dve_Type_Penality_Ctrl.DVE_RAND_COST << 24) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_GMV_COST << 20) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_PREV_COST << 16) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_NBR_COST << 12) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_REFINE_COST << 8) | (pDveConfig->Dve_Type_Penality_Ctrl.DVE_TMPR_COST << 4) | pDveConfig->Dve_Type_Penality_Ctrl.DVE_SPTL_COST;


        dpe_DveConfig.DPE_DVE_IMGI_L_BASE_ADDR =  pDveConfig->Dve_Imgi_l.u4BufPA;
        dpe_DveConfig.DPE_DVE_IMGI_L_STRIDE =  pDveConfig->Dve_Imgi_l.u4Stride;
        dpe_DveConfig.DPE_DVE_IMGI_R_BASE_ADDR =  pDveConfig->Dve_Imgi_r.u4BufPA;
        dpe_DveConfig.DPE_DVE_IMGI_R_STRIDE =  pDveConfig->Dve_Imgi_r.u4Stride;
        dpe_DveConfig.DPE_DVE_DVI_L_BASE_ADDR =  pDveConfig->Dve_Dvi_l.u4BufPA;
        dpe_DveConfig.DPE_DVE_DVI_L_STRIDE =  pDveConfig->Dve_Dvi_l.u4Stride;
        dpe_DveConfig.DPE_DVE_DVI_R_BASE_ADDR =  pDveConfig->Dve_Dvi_r.u4BufPA;
        dpe_DveConfig.DPE_DVE_DVI_R_STRIDE =  pDveConfig->Dve_Dvi_r.u4Stride;
        dpe_DveConfig.DPE_DVE_MASKI_L_BASE_ADDR =  pDveConfig->Dve_Maski_l.u4BufPA;
        dpe_DveConfig.DPE_DVE_MASKI_L_STRIDE =  pDveConfig->Dve_Maski_l.u4Stride;
        dpe_DveConfig.DPE_DVE_MASKI_R_BASE_ADDR =  pDveConfig->Dve_Maski_r.u4BufPA;
        dpe_DveConfig.DPE_DVE_MASKI_R_STRIDE =  pDveConfig->Dve_Maski_r.u4Stride;
        dpe_DveConfig.DPE_DVE_DVO_L_BASE_ADDR =  pDveConfig->Dve_Dvo_l.u4BufPA;
        dpe_DveConfig.DPE_DVE_DVO_L_STRIDE =  pDveConfig->Dve_Dvo_l.u4Stride;
        dpe_DveConfig.DPE_DVE_DVO_R_BASE_ADDR =  pDveConfig->Dve_Dvo_r.u4BufPA;
        dpe_DveConfig.DPE_DVE_DVO_R_STRIDE =  pDveConfig->Dve_Dvo_r.u4Stride;
        dpe_DveConfig.DPE_DVE_CONFO_L_BASE_ADDR =  pDveConfig->Dve_Confo_l.u4BufPA;
        dpe_DveConfig.DPE_DVE_CONFO_L_STRIDE =  pDveConfig->Dve_Confo_l.u4Stride;
        dpe_DveConfig.DPE_DVE_CONFO_R_BASE_ADDR =  pDveConfig->Dve_Confo_r.u4BufPA;
        dpe_DveConfig.DPE_DVE_CONFO_R_STRIDE =  pDveConfig->Dve_Confo_r.u4Stride;
        dpe_DveConfig.DPE_DVE_RESPO_L_BASE_ADDR =  pDveConfig->Dve_Respo_l.u4BufPA;
        dpe_DveConfig.DPE_DVE_RESPO_L_STRIDE =  pDveConfig->Dve_Respo_l.u4Stride;
        dpe_DveConfig.DPE_DVE_RESPO_R_BASE_ADDR =  pDveConfig->Dve_Respo_r.u4BufPA;
        dpe_DveConfig.DPE_DVE_RESPO_R_STRIDE =  pDveConfig->Dve_Respo_r.u4Stride;

        LOG_INF("DPE_DVE_IMGI_L_BASE_ADDR:(0x%x)\n", dpe_DveConfig.DPE_DVE_IMGI_L_BASE_ADDR);
        LOG_INF("DPE_DVE_IMGI_R_BASE_ADDR:(0x%x)\n", dpe_DveConfig.DPE_DVE_IMGI_R_BASE_ADDR);
        LOG_INF("DPE_DVE_DVI_L_BASE_ADDR:(0x%x)\n", dpe_DveConfig.DPE_DVE_DVI_L_BASE_ADDR);
        LOG_INF("DPE_DVE_DVI_R_BASE_ADDR:(0x%x)\n", dpe_DveConfig.DPE_DVE_DVI_R_BASE_ADDR);
        LOG_INF("DPE_DVE_MASKI_L_BASE_ADDR:(0x%x)\n", dpe_DveConfig.DPE_DVE_MASKI_L_BASE_ADDR);
        LOG_INF("DPE_DVE_MASKI_R_BASE_ADDR:(0x%x)\n", dpe_DveConfig.DPE_DVE_MASKI_R_BASE_ADDR);
        LOG_INF("DPE_DVE_DVO_L_BASE_ADDR:(0x%x)\n", dpe_DveConfig.DPE_DVE_DVO_L_BASE_ADDR);
        LOG_INF("DPE_DVE_DVO_R_BASE_ADDR:(0x%x)\n", dpe_DveConfig.DPE_DVE_DVO_R_BASE_ADDR);
        LOG_INF("DPE_DVE_CONFO_L_BASE_ADDR:(0x%x)\n", dpe_DveConfig.DPE_DVE_CONFO_L_BASE_ADDR);
        LOG_INF("DPE_DVE_CONFO_R_BASE_ADDR:(0x%x)\n", dpe_DveConfig.DPE_DVE_CONFO_R_BASE_ADDR);
        LOG_INF("DPE_DVE_RESPO_L_BASE_ADDR:(0x%x)\n", dpe_DveConfig.DPE_DVE_RESPO_L_BASE_ADDR);
        LOG_INF("DPE_DVE_RESPO_R_BASE_ADDR:(0x%x)\n", dpe_DveConfig.DPE_DVE_RESPO_R_BASE_ADDR);


        Ret = ioctl(this->m_Fd,DPE_DVE_ENQUE,&dpe_DveConfig);
        if(Ret < 0)
        {
            LOG_ERR("DPE_DVE_ENQUE_REQ fail(%d)\n", Ret);
            return MFALSE;
        }
    }
#endif

    return MTRUE;


}

MBOOL DpeDrvImp::dequeDVE(vector<NSCam::NSIoPipe::DVEConfig>& DveConfigVec)
{
    MINT32 Ret;
    NSCam::NSIoPipe::DVEConfig DveConfig;
    DPE_DVERequest dpe_dveRequest;
    DPE_DVEConfig dpe_DveConfig[_SUPPORT_MAX_DPE_FRAME_REQUEST_];
    dpe_dveRequest.m_pDpeConfig = dpe_DveConfig;
#if 1
    Ret = ioctl(this->m_Fd,DPE_DVE_DEQUE_REQ,&dpe_dveRequest);
    if(Ret < 0)
    {
        LOG_ERR("DPE_DVE_DEQUE_REQ fail(%d)\n", Ret);
        printf("DPE_DVE_DEQUE_REQ fail(%d)\n", Ret);
        return MFALSE;
    }
    printf("dequeDVE num:%d\n", dpe_dveRequest.m_ReqNum);

    for (unsigned int i=0; i< dpe_dveRequest.m_ReqNum; i++)
    {
        DveConfig.Dve_Horz_Sv = (dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_STA_0 & 0x3ff);
        DveConfig.Dve_Vert_Sv = (dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_STA_0 >> 16) & 0x3f;

        DveConfig.Dve_Imgi_l.u4BufPA = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_IMGI_L_BASE_ADDR;
        DveConfig.Dve_Imgi_l.u4Stride = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_IMGI_L_STRIDE;
        DveConfig.Dve_Imgi_r.u4BufPA = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_IMGI_R_BASE_ADDR;
        DveConfig.Dve_Imgi_r.u4Stride = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_IMGI_R_STRIDE;
        DveConfig.Dve_Dvi_l.u4BufPA = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_DVI_L_BASE_ADDR;
        DveConfig.Dve_Dvi_l.u4Stride = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_DVI_L_STRIDE;
        DveConfig.Dve_Dvi_r.u4BufPA = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_DVI_R_BASE_ADDR;
        DveConfig.Dve_Dvi_r.u4Stride = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_DVI_R_STRIDE;
        DveConfig.Dve_Maski_l.u4BufPA = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_MASKI_L_BASE_ADDR;
        DveConfig.Dve_Maski_l.u4Stride = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_MASKI_L_STRIDE;
        DveConfig.Dve_Maski_r.u4BufPA = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_MASKI_R_BASE_ADDR;
        DveConfig.Dve_Maski_r.u4Stride = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_MASKI_R_STRIDE;

        DveConfig.Dve_Dvo_l.u4BufPA = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_DVO_L_BASE_ADDR;
        DveConfig.Dve_Dvo_l.u4Stride = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_DVO_L_STRIDE;
        DveConfig.Dve_Dvo_r.u4BufPA = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_DVO_R_BASE_ADDR;
        DveConfig.Dve_Dvo_r.u4Stride = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_DVO_R_STRIDE;

        DveConfig.Dve_Confo_l.u4BufPA = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_CONFO_L_BASE_ADDR;
        DveConfig.Dve_Confo_l.u4Stride = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_CONFO_L_STRIDE;
        DveConfig.Dve_Confo_r.u4BufPA = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_CONFO_R_BASE_ADDR;
        DveConfig.Dve_Confo_r.u4Stride = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_CONFO_R_STRIDE;
        DveConfig.Dve_Respo_l.u4BufPA = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_RESPO_L_BASE_ADDR;
        DveConfig.Dve_Respo_l.u4Stride = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_RESPO_L_STRIDE;
        DveConfig.Dve_Respo_r.u4BufPA = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_RESPO_R_BASE_ADDR;
        DveConfig.Dve_Respo_r.u4Stride = dpe_dveRequest.m_pDpeConfig[i].DPE_DVE_RESPO_R_STRIDE;
        DveConfigVec.push_back(DveConfig);

    }

#else
    MINT32 Ret;
    unsigned int num;
    NSCam::NSIoPipe::DVEConfig DveConfig;
    DPE_DVEConfig dpe_DveConfig;
    Ret = ioctl(this->m_Fd,DPE_DVE_DEQUE_NUM,&num);
    if(Ret < 0)
    {
        LOG_ERR("DPE_DVE_DEQUE_NUM fail(%d)\n", Ret);
        printf("DPE_DVE_DEQUE_NUM fail(%d)\n", Ret);
        return MFALSE;
    }
    printf("dequeDVE num:%d\n", num);
    for (unsigned int i=0; i< num; i++)
    {
        Ret = ioctl(this->m_Fd,DPE_DVE_DEQUE,&dpe_DveConfig);
        if(Ret < 0)
        {
            LOG_ERR("DPE_DVE_DEQUE fail(%d)\n", Ret);
            printf("DPE_DVE_DEQUE fail(%d)\n", Ret);
            return MFALSE;
        }
        DveConfig.Dve_Horz_Sv = (dpe_DveConfig.DPE_DVE_STA_0 & 0x3ff);
        DveConfig.Dve_Vert_Sv = (dpe_DveConfig.DPE_DVE_STA_0 >> 16) & 0x3f;

        DveConfig.Dve_Imgi_l.u4BufPA = dpe_DveConfig.DPE_DVE_IMGI_L_BASE_ADDR;
        DveConfig.Dve_Imgi_l.u4Stride = dpe_DveConfig.DPE_DVE_IMGI_L_STRIDE;
        DveConfig.Dve_Imgi_r.u4BufPA = dpe_DveConfig.DPE_DVE_IMGI_R_BASE_ADDR;
        DveConfig.Dve_Imgi_r.u4Stride = dpe_DveConfig.DPE_DVE_IMGI_R_STRIDE;
        DveConfig.Dve_Dvi_l.u4BufPA = dpe_DveConfig.DPE_DVE_DVI_L_BASE_ADDR;
        DveConfig.Dve_Dvi_l.u4Stride = dpe_DveConfig.DPE_DVE_DVI_L_STRIDE;
        DveConfig.Dve_Dvi_r.u4BufPA = dpe_DveConfig.DPE_DVE_DVI_R_BASE_ADDR;
        DveConfig.Dve_Dvi_r.u4Stride = dpe_DveConfig.DPE_DVE_DVI_R_STRIDE;
        DveConfig.Dve_Maski_l.u4BufPA = dpe_DveConfig.DPE_DVE_MASKI_L_BASE_ADDR;
        DveConfig.Dve_Maski_l.u4Stride = dpe_DveConfig.DPE_DVE_MASKI_L_STRIDE;
        DveConfig.Dve_Maski_r.u4BufPA = dpe_DveConfig.DPE_DVE_MASKI_R_BASE_ADDR;
        DveConfig.Dve_Maski_r.u4Stride = dpe_DveConfig.DPE_DVE_MASKI_R_STRIDE;

        DveConfig.Dve_Dvo_l.u4BufPA = dpe_DveConfig.DPE_DVE_DVO_L_BASE_ADDR;
        DveConfig.Dve_Dvo_l.u4Stride = dpe_DveConfig.DPE_DVE_DVO_L_STRIDE;
        DveConfig.Dve_Dvo_r.u4BufPA = dpe_DveConfig.DPE_DVE_DVO_R_BASE_ADDR;
        DveConfig.Dve_Dvo_r.u4Stride = dpe_DveConfig.DPE_DVE_DVO_R_STRIDE;

        DveConfig.Dve_Confo_l.u4BufPA = dpe_DveConfig.DPE_DVE_CONFO_L_BASE_ADDR;
        DveConfig.Dve_Confo_l.u4Stride = dpe_DveConfig.DPE_DVE_CONFO_L_STRIDE;
        DveConfig.Dve_Confo_r.u4BufPA = dpe_DveConfig.DPE_DVE_CONFO_R_BASE_ADDR;
        DveConfig.Dve_Confo_r.u4Stride = dpe_DveConfig.DPE_DVE_CONFO_R_STRIDE;
        DveConfig.Dve_Respo_l.u4BufPA = dpe_DveConfig.DPE_DVE_RESPO_L_BASE_ADDR;
        DveConfig.Dve_Respo_l.u4Stride = dpe_DveConfig.DPE_DVE_RESPO_L_STRIDE;
        DveConfig.Dve_Respo_r.u4BufPA = dpe_DveConfig.DPE_DVE_RESPO_R_BASE_ADDR;
        DveConfig.Dve_Respo_r.u4Stride = dpe_DveConfig.DPE_DVE_RESPO_R_STRIDE;
        DveConfigVec.push_back(DveConfig);

    }
#endif
    return MTRUE;
}

MBOOL DpeDrvImp::enqueWMFE(vector<NSCam::NSIoPipe::WMFEConfig>& WmfeConfigVec)
{

    MINT32 Ret;
    MBOOL scale = MFALSE;
    unsigned int num = WmfeConfigVec.size();
    NSCam::NSIoPipe::WMFEConfig* pWmfeConfig;
    DPE_WMFERequest dpe_wmfeRequest;
    DPE_WMFEConfig dpe_WmfeConfig[_SUPPORT_MAX_DPE_FRAME_REQUEST_];

#if 1

    for (unsigned int i=0; i<WmfeConfigVec.size(); i++)
    {
        pWmfeConfig = &(WmfeConfigVec.at(i));

        //WMFE Config 0
        if (pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpo.u4Stride != pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpi.u4Stride)
        {
            if (pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpo.u4Stride == (2*pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpi.u4Stride))
            {
                scale = MTRUE;
            }
            else
            {
                LOG_ERR("the size of Wmfe_Ctrl_0 Dpo and Dpi is not support!! Dpo Stride:(%d), Dpi Stride:(%d)\n", pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpo.u4Stride, pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpi.u4Stride);
            }
        }
        dpe_WmfeConfig[i].DPE_WMFE_CTRL_0 = (pWmfeConfig->Wmfe_Ctrl_0.WmfeFilterSize<<12) | (pWmfeConfig->Wmfe_Ctrl_0.WmfeDpiFmt<<10) | (pWmfeConfig->Wmfe_Ctrl_0.WmfeImgiFmt<<8) | (1<<5) | (scale << 4) | (pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Enable);
        dpe_WmfeConfig[i].DPE_WMFE_SIZE_0 = (pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Height<<16) | (pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Width);
        dpe_WmfeConfig[i].DPE_WMFE_IMGI_BASE_ADDR_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Imgi.u4BufPA;
        dpe_WmfeConfig[i].DPE_WMFE_IMGI_STRIDE_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Imgi.u4Stride;
        dpe_WmfeConfig[i].DPE_WMFE_DPI_BASE_ADDR_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpi.u4BufPA;
        dpe_WmfeConfig[i].DPE_WMFE_DPI_STRIDE_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpi.u4Stride;
        dpe_WmfeConfig[i].DPE_WMFE_TBLI_BASE_ADDR_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Tbli.u4BufPA;
        dpe_WmfeConfig[i].DPE_WMFE_TBLI_STRIDE_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Tbli.u4Stride;
        dpe_WmfeConfig[i].DPE_WMFE_DPO_BASE_ADDR_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpo.u4BufPA;
        dpe_WmfeConfig[i].DPE_WMFE_DPO_STRIDE_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpo.u4Stride;

        //WMFE Config 1
        scale = MFALSE;
        if (pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpo.u4Stride != pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpi.u4Stride)
        {
            if (pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpo.u4Stride == (2*pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpi.u4Stride))
            {
                scale = MTRUE;
            }
            else
            {
                LOG_ERR("the size of Wmfe_Ctrl_1 Dpo and Dpi is not support!! Dpo Stride:(%d), Dpi Stride:(%d)\n", pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpo.u4Stride, pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpi.u4Stride);
            }
        }
        dpe_WmfeConfig[i].DPE_WMFE_CTRL_1 = (pWmfeConfig->Wmfe_Ctrl_1.WmfeFilterSize<<12) | (pWmfeConfig->Wmfe_Ctrl_1.WmfeDpiFmt<<10) | (pWmfeConfig->Wmfe_Ctrl_1.WmfeImgiFmt<<8) | (1<<5) | (scale << 4) | (pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Enable);
        dpe_WmfeConfig[i].DPE_WMFE_SIZE_1 = (pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Height<<16) | (pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Width);
        dpe_WmfeConfig[i].DPE_WMFE_IMGI_BASE_ADDR_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Imgi.u4BufPA;
        dpe_WmfeConfig[i].DPE_WMFE_IMGI_STRIDE_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Imgi.u4Stride;
        dpe_WmfeConfig[i].DPE_WMFE_DPI_BASE_ADDR_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpi.u4BufPA;
        dpe_WmfeConfig[i].DPE_WMFE_DPI_STRIDE_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpi.u4Stride;
        dpe_WmfeConfig[i].DPE_WMFE_TBLI_BASE_ADDR_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Tbli.u4BufPA;
        dpe_WmfeConfig[i].DPE_WMFE_TBLI_STRIDE_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Tbli.u4Stride;
        dpe_WmfeConfig[i].DPE_WMFE_DPO_BASE_ADDR_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpo.u4BufPA;
        dpe_WmfeConfig[i].DPE_WMFE_DPO_STRIDE_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpo.u4Stride;

        //WMFE Config 2
        scale = MFALSE;
        if (pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpo.u4Stride != pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpi.u4Stride)
        {
            if (pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpo.u4Stride == (2*pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpi.u4Stride))
            {
                scale = MTRUE;
            }
            else
            {
                LOG_ERR("the size of Wmfe_Ctrl_2 Dpo and Dpi is not support!! Dpo Stride:(%d), Dpi Stride:(%d)\n", pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpo.u4Stride, pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpi.u4Stride);
            }
        }
        if ((DPE_DEFAULT_UT == g_DPE_UnitTest_Num) || (DPE_TESTCASE_UT_0 == g_DPE_UnitTest_Num))
        {
            dpe_WmfeConfig[i].DPE_WMFE_CTRL_2 = (pWmfeConfig->Wmfe_Ctrl_2.WmfeFilterSize<<12) | (pWmfeConfig->Wmfe_Ctrl_2.WmfeDpiFmt<<10) | (pWmfeConfig->Wmfe_Ctrl_2.WmfeImgiFmt<<8) | (1<<5)  | (1<<4) | (pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Enable);
        }
        else
        {
            dpe_WmfeConfig[i].DPE_WMFE_CTRL_2 = (pWmfeConfig->Wmfe_Ctrl_2.WmfeFilterSize<<12) | (pWmfeConfig->Wmfe_Ctrl_2.WmfeDpiFmt<<10) | (pWmfeConfig->Wmfe_Ctrl_2.WmfeImgiFmt<<8) | (1<<5)  | (scale << 4) | (pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Enable);
        }
        dpe_WmfeConfig[i].DPE_WMFE_SIZE_2 = (pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Height<<16) | (pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Width);
        dpe_WmfeConfig[i].DPE_WMFE_IMGI_BASE_ADDR_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Imgi.u4BufPA;
        dpe_WmfeConfig[i].DPE_WMFE_IMGI_STRIDE_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Imgi.u4Stride;
        dpe_WmfeConfig[i].DPE_WMFE_DPI_BASE_ADDR_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpi.u4BufPA;
        dpe_WmfeConfig[i].DPE_WMFE_DPI_STRIDE_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpi.u4Stride;
        dpe_WmfeConfig[i].DPE_WMFE_TBLI_BASE_ADDR_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Tbli.u4BufPA;
        dpe_WmfeConfig[i].DPE_WMFE_TBLI_STRIDE_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Tbli.u4Stride;
        dpe_WmfeConfig[i].DPE_WMFE_DPO_BASE_ADDR_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpo.u4BufPA;
        dpe_WmfeConfig[i].DPE_WMFE_DPO_STRIDE_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpo.u4Stride;

    }

    dpe_wmfeRequest.m_ReqNum = num;
    dpe_wmfeRequest.m_pWmfeConfig = dpe_WmfeConfig;

    Ret = ioctl(this->m_Fd,DPE_WMFE_ENQUE_REQ,&dpe_wmfeRequest);
    if(Ret < 0)
    {
        LOG_ERR("DPE_WMFE_ENQUE_REQ fail(%d)\n", Ret);
        return MFALSE;
    }


#else
    MINT32 Ret;
    unsigned int num = WmfeConfigVec.size();
    NSCam::NSIoPipe::WMFEConfig* pWmfeConfig;
    DPE_WMFEConfig dpe_WmfeConfig;
    Ret = ioctl(this->m_Fd,DPE_WMFE_ENQNUE_NUM,&num);
    if(Ret < 0)
    {
        LOG_ERR("DPE_WMFE_EQNUE_NUM fail(%d)\n", Ret);
        return MFALSE;
    }

    for (unsigned int i=0; i<WmfeConfigVec.size(); i++)
    {
        pWmfeConfig = &(WmfeConfigVec.at(i));

        //WMFE Config 0
        dpe_WmfeConfig.DPE_WMFE_CTRL_0 = (pWmfeConfig->Wmfe_Ctrl_0.WmfeFilterSize<<12) | (pWmfeConfig->Wmfe_Ctrl_0.WmfeDpiFmt<<10) | (pWmfeConfig->Wmfe_Ctrl_0.WmfeImgiFmt<<8) | (1<<5) | (pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Enable);
        dpe_WmfeConfig.DPE_WMFE_SIZE_0 = (pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Height<<16) | (pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Width);
        dpe_WmfeConfig.DPE_WMFE_IMGI_BASE_ADDR_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Imgi.u4BufPA;
        dpe_WmfeConfig.DPE_WMFE_IMGI_STRIDE_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Imgi.u4Stride;
        dpe_WmfeConfig.DPE_WMFE_DPI_BASE_ADDR_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpi.u4BufPA;
        dpe_WmfeConfig.DPE_WMFE_DPI_STRIDE_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpi.u4Stride;
        dpe_WmfeConfig.DPE_WMFE_TBLI_BASE_ADDR_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Tbli.u4BufPA;
        dpe_WmfeConfig.DPE_WMFE_TBLI_STRIDE_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Tbli.u4Stride;
        dpe_WmfeConfig.DPE_WMFE_DPO_BASE_ADDR_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpo.u4BufPA;
        dpe_WmfeConfig.DPE_WMFE_DPO_STRIDE_0 = pWmfeConfig->Wmfe_Ctrl_0.Wmfe_Dpo.u4Stride;

        //WMFE Config 1
        dpe_WmfeConfig.DPE_WMFE_CTRL_1 = (pWmfeConfig->Wmfe_Ctrl_1.WmfeFilterSize<<12) | (pWmfeConfig->Wmfe_Ctrl_1.WmfeDpiFmt<<10) | (pWmfeConfig->Wmfe_Ctrl_1.WmfeImgiFmt<<8) | (1<<5) | (pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Enable);
        dpe_WmfeConfig.DPE_WMFE_SIZE_1 = (pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Height<<16) | (pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Width);
        dpe_WmfeConfig.DPE_WMFE_IMGI_BASE_ADDR_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Imgi.u4BufPA;
        dpe_WmfeConfig.DPE_WMFE_IMGI_STRIDE_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Imgi.u4Stride;
        dpe_WmfeConfig.DPE_WMFE_DPI_BASE_ADDR_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpi.u4BufPA;
        dpe_WmfeConfig.DPE_WMFE_DPI_STRIDE_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpi.u4Stride;
        dpe_WmfeConfig.DPE_WMFE_TBLI_BASE_ADDR_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Tbli.u4BufPA;
        dpe_WmfeConfig.DPE_WMFE_TBLI_STRIDE_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Tbli.u4Stride;
        dpe_WmfeConfig.DPE_WMFE_DPO_BASE_ADDR_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpo.u4BufPA;
        dpe_WmfeConfig.DPE_WMFE_DPO_STRIDE_1 = pWmfeConfig->Wmfe_Ctrl_1.Wmfe_Dpo.u4Stride;

        //WMFE Config 2
        if ((DPE_DEFAULT_UT == g_DPE_UnitTest_Num) || (DPE_TESTCASE_UT_0 == g_DPE_UnitTest_Num))
        {
            dpe_WmfeConfig.DPE_WMFE_CTRL_2 = (pWmfeConfig->Wmfe_Ctrl_2.WmfeFilterSize<<12) | (pWmfeConfig->Wmfe_Ctrl_2.WmfeDpiFmt<<10) | (pWmfeConfig->Wmfe_Ctrl_2.WmfeImgiFmt<<8) | (1<<5)  | (1<<4) | (pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Enable);
        }
        else
        {
            dpe_WmfeConfig.DPE_WMFE_CTRL_2 = (pWmfeConfig->Wmfe_Ctrl_2.WmfeFilterSize<<12) | (pWmfeConfig->Wmfe_Ctrl_2.WmfeDpiFmt<<10) | (pWmfeConfig->Wmfe_Ctrl_2.WmfeImgiFmt<<8) | (1<<5) | (pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Enable);
        }
        dpe_WmfeConfig.DPE_WMFE_SIZE_2 = (pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Height<<16) | (pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Width);
        dpe_WmfeConfig.DPE_WMFE_IMGI_BASE_ADDR_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Imgi.u4BufPA;
        dpe_WmfeConfig.DPE_WMFE_IMGI_STRIDE_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Imgi.u4Stride;
        dpe_WmfeConfig.DPE_WMFE_DPI_BASE_ADDR_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpi.u4BufPA;
        dpe_WmfeConfig.DPE_WMFE_DPI_STRIDE_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpi.u4Stride;
        dpe_WmfeConfig.DPE_WMFE_TBLI_BASE_ADDR_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Tbli.u4BufPA;
        dpe_WmfeConfig.DPE_WMFE_TBLI_STRIDE_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Tbli.u4Stride;
        dpe_WmfeConfig.DPE_WMFE_DPO_BASE_ADDR_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpo.u4BufPA;
        dpe_WmfeConfig.DPE_WMFE_DPO_STRIDE_2 = pWmfeConfig->Wmfe_Ctrl_2.Wmfe_Dpo.u4Stride;

        Ret = ioctl(this->m_Fd,DPE_WMFE_ENQUE,&dpe_WmfeConfig);
        if(Ret < 0)
        {
            LOG_ERR("DPE_WMFE_ENQUE_REQ fail(%d)\n", Ret);
            return MFALSE;
        }
    }
#endif
    return MTRUE;


}

MBOOL DpeDrvImp::dequeWMFE(vector<NSCam::NSIoPipe::WMFEConfig>& WmfeConfigVec)
{
    MINT32 Ret;
    NSCam::NSIoPipe::WMFEConfig WmfeConfig;
    DPE_WMFERequest dpe_wmfeRequest;
    DPE_WMFEConfig dpe_WmfeConfig[_SUPPORT_MAX_DPE_FRAME_REQUEST_];
    dpe_wmfeRequest.m_pWmfeConfig = dpe_WmfeConfig;
#if 1
    Ret = ioctl(this->m_Fd,DPE_WMFE_DEQUE_REQ,&dpe_wmfeRequest);
    if(Ret < 0)
    {
        LOG_ERR("DPE_WMFE_DEQUE_REQ fail(%d)\n", Ret);
        printf("DPE_WMFE_DEQUE_REQ fail(%d)\n", Ret);
        return MFALSE;
    }
    printf("dequeWMFE num:%d\n", dpe_wmfeRequest.m_ReqNum);
    for (unsigned int i=0; i< dpe_wmfeRequest.m_ReqNum; i++)
    {
        WmfeConfigVec.push_back(WmfeConfig);
    }

#else
    MINT32 Ret;
    unsigned int num;
    NSCam::NSIoPipe::WMFEConfig WmfeConfig;
    DPE_WMFEConfig dpe_WmfeConfig;
    Ret = ioctl(this->m_Fd,DPE_WMFE_DEQUE_NUM,&num);
    if(Ret < 0)
    {
        LOG_ERR("DPE_WMFE_DEQUE_NUM fail(%d)\n", Ret);
        printf("DPE_WMFE_DEQUE_NUM fail(%d)\n", Ret);
        return MFALSE;
    }
    printf("dequeWMFE num:%d\n", num);
    for (unsigned int i=0; i< num; i++)
    {
        Ret = ioctl(this->m_Fd,DPE_WMFE_DEQUE,&dpe_WmfeConfig);
        if(Ret < 0)
        {
            LOG_ERR("DPE_WMFE_DEQUE fail(%d)\n", Ret);
            printf("DPE_WMFE_DEQUE fail(%d)\n", Ret);
            return MFALSE;
        }
        WmfeConfigVec.push_back(WmfeConfig);
    }
#endif

    return MTRUE;
}


MUINT32 DpeDrvImp::readReg(MUINT32 Addr,MINT32 caller)
{
    MINT32 Ret=0;
    MUINT32 value=0x0;
    MUINT32 legal_range = DPE_REG_RANGE;
    LOG_DBG("+,Dpe_read:Addr(0x%x)\n",Addr);
    android::Mutex::Autolock lock(this->DpeRegMutex);
    //(void)caller;
    if(-1 == FD_CHK()){
        return 1;
    }


    if(this->m_regRWMode==DPE_DRV_RW_MMAP){
        if(Addr >= legal_range){
            LOG_ERR("over range(0x%x)\n",Addr);
            return 0;
        }
        value = this->m_pDpeHwRegAddr[(Addr>>2)];
    }
    else{
        DPE_REG_IO_STRUCT DpeRegIo;
        DPE_DRV_REG_IO_STRUCT RegIo;
        //RegIo.module = this->m_HWmodule;
        RegIo.Addr = Addr;
        DpeRegIo.pData = (DPE_REG_STRUCT*)&RegIo;
        DpeRegIo.Count = 1;

        Ret = ioctl(this->m_Fd, DPE_READ_REGISTER, &DpeRegIo);
        if(Ret < 0)
        {
            LOG_ERR("DPE_READ via IO fail(%d)", Ret);
            return value;
        }
        value=RegIo.Data;
    }
    LOG_DBG("-,Dpe_read:(0x%x,0x%x)",Addr,value);
    return value;
}

//-----------------------------------------------------------------------------
MBOOL DpeDrvImp::readRegs(DPE_DRV_REG_IO_STRUCT*  pRegIo,MUINT32 Count,MINT32 caller)
{
    MINT32 Ret;
    MUINT32 legal_range = DPE_REG_RANGE;
    android::Mutex::Autolock lock(this->DpeRegMutex);
    //(void)caller;
    if((-1 == FD_CHK()) || (NULL == pRegIo)){
        return MFALSE;
    }

    if(this->m_regRWMode == DPE_DRV_RW_MMAP){
        unsigned int i;
        for (i=0; i<Count; i++)
        {
            if(pRegIo[i].Addr >= legal_range)
            {
                LOG_ERR("over range,bypass_0x%x\n",pRegIo[i].Addr);
            }
            else
            {
                pRegIo[i].Data = this->m_pDpeHwRegAddr[(pRegIo[i].Addr>>2)];
            }
        }
    }
    else{
        DPE_REG_IO_STRUCT DpeRegIo;
        //pRegIo->module = this->m_HWmodule;
        DpeRegIo.pData = (DPE_REG_STRUCT*)pRegIo;
        DpeRegIo.Count = Count;


        Ret = ioctl(this->m_Fd, DPE_READ_REGISTER, &DpeRegIo);
        if(Ret < 0)
        {
            LOG_ERR("DPE_READ via IO fail(%d)", Ret);
            return MFALSE;
        }
    }

    LOG_DBG("Dpe_reads_Cnt(%d): 0x%x_0x%x", Count, pRegIo[0].Addr,pRegIo[0].Data);
    return MTRUE;
}


MBOOL DpeDrvImp::writeReg(MUINT32 Addr,unsigned long Data,MINT32 caller)
{
    MINT32 ret=0;
    MUINT32 legal_range = DPE_REG_RANGE;
    LOG_DBG("Dpe_write:m_regRWMode(0x%x),(0x%x,0x%x)",this->m_regRWMode,Addr,Data);
    android::Mutex::Autolock lock(this->DpeRegMutex);
    //(void)caller;
    if(-1 == FD_CHK()){
        return MFALSE;
    }

    switch(this->m_regRWMode){
        case DPE_DRV_RW_MMAP:
            if(Addr >= legal_range){
                LOG_ERR("over range(0x%x)\n",Addr);
                return MFALSE;
            }
            this->m_pDpeHwRegAddr[(Addr>>2)] = Data;
            break;
        case DPE_DRV_RW_IOCTL:
            DPE_REG_IO_STRUCT DpeRegIo;
            DPE_DRV_REG_IO_STRUCT RegIo;
            //RegIo.module = this->m_HWmodule;
            RegIo.Addr = Addr;
            RegIo.Data = Data;
            DpeRegIo.pData = (DPE_REG_STRUCT*)&RegIo;
            DpeRegIo.Count = 1;
            ret = ioctl(this->m_Fd, DPE_WRITE_REGISTER, &DpeRegIo);
            if(ret < 0){
                LOG_ERR("DPE_WRITE via IO fail(%d)", ret);
                return MFALSE;
            }
            break;
        case DPE_DRV_R_ONLY:
            LOG_ERR("DPE Read Only");
            return MFALSE;
            break;
        default:
            LOG_ERR("no this reg operation mode(0x%x)",this->m_regRWMode);
            return MFALSE;
            break;
    }
    //
    //release mutex in order to read back for DBG log
    this->DpeRegMutex.unlock();
    //
    return MTRUE;
}

MBOOL DpeDrvImp::writeRegs(DPE_DRV_REG_IO_STRUCT*  pRegIo,MUINT32 Count,MINT32 caller)
{
    MINT32 Ret;
    unsigned int i=0;
    MUINT32 legal_range = DPE_REG_RANGE;
    android::Mutex::Autolock lock(this->DpeRegMutex);
    //(void)caller;
    if(-1 == FD_CHK() || (NULL == pRegIo)){
        return MFALSE;
    }

    switch(this->m_regRWMode){
        case DPE_DRV_RW_IOCTL:
            DPE_REG_IO_STRUCT DpeRegIo;
            //pRegIo->module = this->m_HWmodule;
            DpeRegIo.pData = (DPE_REG_STRUCT*)pRegIo;
            DpeRegIo.Count = Count;

            Ret = ioctl(this->m_Fd, DPE_WRITE_REGISTER, &DpeRegIo);
            if(Ret < 0){
                LOG_ERR("DPE_WRITE via IO fail(%d)",Ret);
                return MFALSE;
            }
            break;
        case DPE_DRV_RW_MMAP:
            //if(this->m_HWmodule >= CAM_MAX )
            //    legal_range = DIP_BASE_RANGE_SPECIAL;
            do{
                if(pRegIo[i].Addr >= legal_range){
                    LOG_ERR("mmap over range,bypass_0x%x\n",pRegIo[i].Addr);
                    i = Count;
                }
                else
                    this->m_pDpeHwRegAddr[(pRegIo[i].Addr>>2)] = pRegIo[i].Data;
            }while(++i<Count);
            break;
        case DPE_DRV_R_ONLY:
            LOG_ERR("DPE Read Only");
            return MFALSE;
            break;
        default:
            LOG_ERR("no this reg operation mode(0x%x)",this->m_regRWMode);
            return MFALSE;
            break;
    }
    LOG_DBG("Dpe_writes(%d):0x%x_0x%x\n",Count,pRegIo[0].Addr,pRegIo[0].Data);
    return MTRUE;

}

MUINT32 DpeDrvImp::getRWMode(void)
{
    return this->m_regRWMode;
}

MBOOL DpeDrvImp::setRWMode(DPE_DRV_RW_MODE rwMode)
{
    if(rwMode > DPE_DRV_R_ONLY)
    {
        LOG_ERR("no this reg operation mode(0x%x)",this->m_regRWMode);
        return MFALSE;
    }

    this->m_regRWMode = rwMode;
    return MTRUE;
}

