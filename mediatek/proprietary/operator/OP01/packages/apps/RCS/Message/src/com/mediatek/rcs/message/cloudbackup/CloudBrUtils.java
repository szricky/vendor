package com.mediatek.rcs.message.cloudbackup;

/**
 * utils class.
 */
public class CloudBrUtils {
    public static final String MODULE_TAG = "message.cloudbackup/";
    private static final String CLASS_TAG = MODULE_TAG + "CloudBrUtils";

    /**
     * Result code during backup and restore.
     * result dialog will according to it to show result.
     */
    public static class ResultCode {
        public static final int IO_EXCEPTION = -1;
        public static final int OK = 0;
        public static final int INSERTDB_EXCEPTION = -2;
        public static final int DB_EXCEPTION = -3;
        public static final int BACKUP_FILE_ERROR = -4;
        public static final int PARSE_XML_ERROR = -5;
        public static final int SERVICE_CANCELED = -6;
        public static final int NETWORK_ERROR = -7;
        public static final int BACKUP_BEFOR_RESTORE_EXCEPTION = -8;
        public static final int BACKUP_FOLDER_EMPTY = -9;
        public static final int OTHER_EXCEPTION = -10;
    }

    public static final String STORAGE_PATH = "/storage/sdcard0/cloudBackup";
    public static final String BACKUP_RESULT = "backupResult";
    public static final String RESTORE_RESULT = "restoreResult";
    public static final String MESSAGE_PATH = "messagePath";
    public static final String FAVORITE_PATH = "favoritePath";

    /**
     * MessageID
     *
     */
    public static class MessageID {
        public static final int BACKUP_END = 0x10;
        public static final int RESTORE_END = 0x11;
        public static final int CANCEL_END = 0x12;
        public static final int PRESS_BACK = 0X501;
    }
}
