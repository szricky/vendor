package com.mediatek.rcs.messageservice.cloudbackup;

import android.content.ContentResolver;
import android.content.Context;
import android.os.PowerManager;
import android.os.RemoteException;
import android.util.Log;

import com.mediatek.rcs.messageservice.cloudbackup.aidl.IMsgBRListener;
import com.mediatek.rcs.messageservice.cloudbackup.modules.FavMsgBackup;
import com.mediatek.rcs.messageservice.cloudbackup.modules.FavMsgRestore;
import com.mediatek.rcs.messageservice.cloudbackup.modules.MessageBackupComposer;
import com.mediatek.rcs.messageservice.cloudbackup.modules.MessageRestoreComposer;
import com.mediatek.rcs.messageservice.cloudbackup.utils.FileUtils;
import com.mediatek.rcs.messageservice.cloudbackup.utils.CloudBrUtils;

import java.io.File;
/**
 * This thread is used to restore favorite and message data download from network.
 */
public class CloudMsgRestoreThread extends Thread {
    private final String CLASS_TAG = CloudBrUtils.MODULE_TAG + "CloudMsgRestoreThread";

    private IMsgBRListener mIMsgRestoreListener;
    private Context mContext;
    protected PowerManager.WakeLock mWakeLock;
    private boolean mCancel = false;

    private String mResotreDataFolder;
    private MessageRestoreComposer mMessageRestoreComposer;
    private FavMsgRestore mFavMsgRestore;
    private FavMsgBackup mFavMsgBackup;
    private MessageBackupComposer mMessageBackupComposer;
    private boolean mIsNeedRestoreOrigData = false;
    private String mMessageBackupFolderPath = FileUtils.ModulePath.RESTORE_BACKUP_FOLDER
            + CloudBrUtils.RemoteFolderName.MSG_BACKUP;
    private String mFavoriteBackupFolderPath = FileUtils.ModulePath.RESTORE_BACKUP_FOLDER
            + CloudBrUtils.RemoteFolderName.MSG_FAVORITE;

    /**
     * set restore folder path and listener.
     * @param FolderPath data path to restore.
     * @param excuteListener used to callback.
     */
    public void setRestoreParam(String FolderPath, IMsgBRListener excuteListener) {
        mResotreDataFolder = FolderPath;
        mIMsgRestoreListener = excuteListener;
    }

    /**
     * set thread cancel.
     * @param cancel ture means calcel.
     */
    public void setCancel(boolean cancel) {
        Log.d(CLASS_TAG, "setCancel = " + cancel);
        mCancel = cancel;
        if (mFavMsgBackup != null) {
            mFavMsgBackup.setCancel(cancel);
        }
        if (mMessageRestoreComposer != null) {
            mMessageRestoreComposer.setCancel(cancel);
        }

        if (mFavMsgRestore != null) {
            mFavMsgRestore.setCancel(cancel);
        }
    }

    public void setContext(Context context) {
        mContext = context;
    }

    @Override
    public void run() {
        if (mIMsgRestoreListener == null || mResotreDataFolder == null) {
            Log.e(CLASS_TAG,
                    "mIMsgRestoreListener == null || mFolderPath ==null");
            Log.e(CLASS_TAG, "mFolderPath = " + mResotreDataFolder);
            return;
        }

        createWakeLock();
        if (mWakeLock != null) {
            acquireWakeLock();
            Log.d(CLASS_TAG, "RestoreService : startRestore: call acquireWakeLock()");
        }

        int restoreResult = CloudBrUtils.ResultCode.OK;
        if (mCancel) {
            Log.d(CLASS_TAG, "BackupThread is be canceled. return");
            sendRestoreResult(CloudBrUtils.ResultCode.SERVICE_CANCELED);
            return;
        }
        restoreResult = prepareForRestore();
        if (restoreResult != CloudBrUtils.ResultCode.OK) {
            Log.e(CLASS_TAG,
                    "backup begore restore error delete temp data , return");
            File tempFolder = new File(
                    FileUtils.ModulePath.RESTORE_BACKUP_FOLDER);
            if (tempFolder != null && tempFolder.exists()) {
                FileUtils.deleteFileOrFolder(tempFolder);
            }
            sendRestoreResult(CloudBrUtils.ResultCode.BACKUP_BEFOR_RESTORE_EXCEPTION);
        }
        Log.d(CLASS_TAG, "backup data befor restore end, begin delete db");
        clearMessageDb();
        mIsNeedRestoreOrigData = true;

        String favoriteDataFolder = mResotreDataFolder + File.separator
                + CloudBrUtils.RemoteFolderName.MSG_FAVORITE;
        String messageDataFolder = mResotreDataFolder + File.separator
                + CloudBrUtils.RemoteFolderName.MSG_BACKUP;

        Log.d(CLASS_TAG, "restoreThread  restore message begin");
        File messageFolder = new File(messageDataFolder);
        if (messageFolder != null && messageFolder.exists()) {
            mMessageRestoreComposer = new MessageRestoreComposer(mContext,
                    messageFolder);
            restoreResult = mMessageRestoreComposer.restoreData();
            if (restoreResult != CloudBrUtils.ResultCode.OK) {
                Log.e(CLASS_TAG,
                        "BackupThread, mMessageRestoreComposer.restoreData() error, return");
                sendRestoreResult(restoreResult);
                return;
            }
        } else {
            Log.d(CLASS_TAG, "messageFolder is not existes");
        }
        mMessageRestoreComposer = null;
        Log.d(CLASS_TAG, "restoreThread  restore message end");

        Log.d(CLASS_TAG, "restoreThread  restore favorite begin");
        File favoriteFolder = new File(favoriteDataFolder);
        if (favoriteFolder != null && favoriteFolder.exists()) {
            mFavMsgRestore = new FavMsgRestore(mContext, favoriteFolder);
            restoreResult = mFavMsgRestore.restoreData(favoriteFolder);
            if (restoreResult != CloudBrUtils.ResultCode.OK) {
                Log.e(CLASS_TAG,
                        "BackupThread, mFavMsgRestore.restoreData(favoriteFolder) error, return");
                sendRestoreResult(restoreResult);
                return;
            }
        } else {
            Log.d(CLASS_TAG, "favoriteFolder is not existes");
        }
        mFavMsgRestore = null;
        Log.d(CLASS_TAG, "restoreThread, restore favorite end");
        mIsNeedRestoreOrigData = false;
        sendRestoreResult(CloudBrUtils.ResultCode.OK);
        Log.d(CLASS_TAG, "RestoreThread backup end, return");
    }

    private int prepareForRestore() {
        Log.d(CLASS_TAG, "RestoreThread, prepareForRestore");

        String messageFolderPath = mMessageBackupFolderPath;
        Log.d(CLASS_TAG, "messageFolderPath = " + messageFolderPath);
        mMessageBackupComposer = new MessageBackupComposer(mContext,
                messageFolderPath);
        int backupResult = mMessageBackupComposer.backupData();

        if (backupResult != CloudBrUtils.ResultCode.OK) {
            Log.e(CLASS_TAG,
                    "BackupThread, mMessageBackupComposer.backupData error, return");
            sendRestoreResult(backupResult);
            return backupResult;
        }
        mMessageBackupComposer = null;

        if (mCancel) {
            Log.d(CLASS_TAG, "BackupThread is be canceled. return");
            backupResult = CloudBrUtils.ResultCode.SERVICE_CANCELED;
            sendRestoreResult(backupResult);
        }

        String favoriteFolderPath = mFavoriteBackupFolderPath;
        Log.d(CLASS_TAG, "favoriteFolderPath = " + favoriteFolderPath);
        mFavMsgBackup = new FavMsgBackup(mContext, favoriteFolderPath);
        backupResult = mFavMsgBackup.backupData();
        if (backupResult != CloudBrUtils.ResultCode.OK) {
            Log.e(CLASS_TAG,
                    "BackupThread, mFavMsgBackup.backupData error, return");
            sendRestoreResult(backupResult);
            return backupResult;
        }
        mFavMsgBackup = null;
        Log.d(CLASS_TAG, "BackupThread, backup favorite msg end,result ok");

        if (mCancel) {
            Log.d(CLASS_TAG, "BackupThread is be canceled. return");
            backupResult = CloudBrUtils.ResultCode.SERVICE_CANCELED;
            sendRestoreResult(backupResult);
        }

        return backupResult;
    }

    private void sendRestoreResult(int restoreResult) {
        if (restoreResult != CloudBrUtils.ResultCode.OK
                && mIsNeedRestoreOrigData) {
            Log.e(CLASS_TAG, "restore error, begin restore original data");
            restoreOrignDd();
        }

        File backupTempFile = new File(
                FileUtils.ModulePath.RESTORE_BACKUP_FOLDER);
        if (backupTempFile != null && backupTempFile.exists()) {
            Log.d(CLASS_TAG, "sendRestoreResult delete temp folder");
            FileUtils.deleteFileOrFolder(backupTempFile);
        }

        File backupFile = new File(mResotreDataFolder);
        if (backupFile != null && backupFile.exists()) {
            Log.d(CLASS_TAG, "sendRestoreResult delete backupFile folder = "
                    + backupFile.getAbsolutePath());
            FileUtils.deleteFileOrFolder(backupFile);
        }

        try {
            mIMsgRestoreListener.onRestoreResult(restoreResult);
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }
        mResotreDataFolder = null;
        mIMsgRestoreListener = null;
        mCancel = false;

        if (mWakeLock != null) {
            releaseWakeLock();
            Log.d(CLASS_TAG, "onDestroy(): call releseWakeLock()");
        }
    }

    private void restoreOrignDd() {
        Log.d(CLASS_TAG, "restoreThread  restore  original message begin");
        int restoreResult = CloudBrUtils.ResultCode.OK;
        clearMessageDb();

        File messageFolder = new File(mMessageBackupFolderPath);
        if (messageFolder != null && messageFolder.exists()) {
            mMessageRestoreComposer = new MessageRestoreComposer(mContext,
                    messageFolder);
            restoreResult = mMessageRestoreComposer.restoreData();
            if (restoreResult != CloudBrUtils.ResultCode.OK) {
                Log.e(CLASS_TAG,
                        "messageFolder, mMessageRestoreComposer.restoreData() error");
            }
        }

        Log.d(CLASS_TAG, "restoreThread  restore favorite begin");
        File favoriteFolder = new File(mFavoriteBackupFolderPath);
        if (favoriteFolder != null && favoriteFolder.exists()) {
            mFavMsgRestore = new FavMsgRestore(mContext, favoriteFolder);
            restoreResult = mFavMsgRestore.restoreData(favoriteFolder);
            if (restoreResult != CloudBrUtils.ResultCode.OK) {
                Log.e(CLASS_TAG,
                        "BackupThread, mFavMsgRestore.restoreData(favoriteFolder) error, return");
            }
        }
        mFavMsgRestore = null;
        Log.d(CLASS_TAG, "restoreThread, restore original end");
    }

    protected synchronized void createWakeLock() {
        // Create a new wake lock if we haven't made one yet.
        if (mWakeLock == null) {
            PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    "RestoreService");
            mWakeLock.setReferenceCounted(false);
            Log.d(CLASS_TAG, "createWakeLock");
        }
    }

    protected void acquireWakeLock() {
        // It's okay to double-acquire this because we are not using it
        // in reference-counted mode.
        mWakeLock.acquire();
        Log.d(CLASS_TAG, "acquireWakeLock");
    }

    protected void releaseWakeLock() {
        // Don't release the wake lock if it hasn't been created and acquired.
        if (mWakeLock != null && mWakeLock.isHeld()) {
            mWakeLock.release();
            mWakeLock = null;
            Log.d(CLASS_TAG, "releaseWakeLock");
        }
        Log.d(CLASS_TAG, "releaseLock");
    }

    private boolean clearMessageDb() {
        ContentResolver contentResolver = mContext.getContentResolver();
        int count = contentResolver.delete(CloudBrUtils.CHAT_CONTENT_URI, null,
                null);
        Log.d(CLASS_TAG, "delete message table entre count = " + count);

        count = contentResolver.delete(CloudBrUtils.RCS_URI, null, null);
        Log.d(CLASS_TAG, "delete rcs message table entre count = " + count);

        count = contentResolver.delete(CloudBrUtils.CHAT_MULTI_URI, null, null);
        Log.d(CLASS_TAG, "delete  multi table entre count = " + count);

        Log.d(CLASS_TAG, "use remote service to delete  sms mms table begin");
        count = 0;

        count = contentResolver.delete(CloudBrUtils.MMS_SMS_URI, null, null);
        Log.d(CLASS_TAG, "delete  sms mms table entre count = " + count);

        count = contentResolver.delete(CloudBrUtils.FAVOTIRE_URI, null, null);
        Log.d(CLASS_TAG, "delete  favorite table entre count = " + count);

        count = contentResolver.delete(CloudBrUtils.FT_URI, null, null);
        Log.d(CLASS_TAG, "delete  ft table entre count = " + count);

        count = contentResolver.delete(CloudBrUtils.SPAM_URI, null, null);
        Log.d(CLASS_TAG, "delete spam entre count = " + count);
        return true;
    }
}
