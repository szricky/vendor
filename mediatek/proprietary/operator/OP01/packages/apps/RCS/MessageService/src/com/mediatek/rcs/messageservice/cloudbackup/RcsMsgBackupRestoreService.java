package com.mediatek.rcs.messageservice.cloudbackup;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.text.TextUtils;
import android.provider.Telephony.Mms;
import android.util.Log;

import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.MultimediaMessagePdu;
import com.google.android.mms.pdu.SendReq;
import com.google.android.mms.pdu.PduParser;
import com.google.android.mms.pdu.PduPersister;
import com.mediatek.rcs.messageservice.cloudbackup.aidl.IRcsMsgBackupRestoreService;
import com.mediatek.rcs.messageservice.cloudbackup.aidl.IMsgBRListener;
import com.mediatek.rcs.messageservice.cloudbackup.utils.CloudBrUtils;
import org.gsma.joyn.JoynServiceConfiguration;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
/**
 * This class used to backup and restore message.
 */
public class RcsMsgBackupRestoreService extends Service {

    private final String CLASS_TAG = CloudBrUtils.MODULE_TAG + "RcsMsgBackupRestoreService";

    private RcsMessageBinder mRcsMessageBinder;
    private Context mContext;
    private CloudMsgRestoreThread mCloudMsgRestoreThread;
    private CloudBackupMsgThread mCloudBackupMsgThread;

    @Override
    public void onCreate() {
        Log.d(CLASS_TAG, "onCreate");
        super.onCreate();
        mRcsMessageBinder = new RcsMessageBinder();
        mContext = this;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        Log.d(CLASS_TAG, "onBind");
        return mRcsMessageBinder;
    }

    /**
     * This binder is used to backup and restore cloud message.
     * also used to open mms in fovorite.
     */
    public class RcsMessageBinder extends IRcsMsgBackupRestoreService.Stub {

        @Override
        public int delMsg(Uri uri) throws android.os.RemoteException {
            return mContext.getContentResolver().delete(uri, null, null);
        }

        @Override
        public void setCancel(boolean isCancel)
                throws android.os.RemoteException {
            if (mCloudMsgRestoreThread != null && mCloudMsgRestoreThread.isAlive()) {
                mCloudMsgRestoreThread.setCancel(isCancel);
            }
            if (mCloudMsgRestoreThread != null && mCloudBackupMsgThread.isAlive()) {
                mCloudBackupMsgThread.setCancel(isCancel);
            }
        }

        @Override
        public boolean restoreCloudMsg(String filePath, IMsgBRListener excuteListener) {
            if (excuteListener == null) {
                Log.e(CLASS_TAG, "excuteListener == null,return fail");
                return false;
            }
            Log.d(CLASS_TAG, "restoreCloudMsg pduPath = " + filePath);
            if (filePath == null) {
                Log.e(CLASS_TAG, "restoreCloudMsg filePath is null,return fail");
                return false;
            }
            File file = new File(filePath);
            if (file == null || !file.exists()) {
                Log.e(CLASS_TAG,
                        "restoreCloudMsg filePath is null or not existed,return fail");
                return false;
            }

            //set MyNumber to used
            CloudBrUtils.setMyNumber(getMyMuber());
            mCloudMsgRestoreThread = new CloudMsgRestoreThread();
            mCloudMsgRestoreThread.setRestoreParam(filePath, excuteListener);
            mCloudMsgRestoreThread.setContext(mContext);
            mCloudMsgRestoreThread.start();
            return true;
        }

        @Override
        public boolean backupCloudMsg(String filePath, IMsgBRListener excuteListener) {
            if (excuteListener == null) {
                Log.e(CLASS_TAG, "excuteListener == null,return fail");
                return false;
            }

            CloudBrUtils.setMyNumber(getMyMuber());
            mCloudBackupMsgThread = new CloudBackupMsgThread();
            mCloudBackupMsgThread.setBackupParam(filePath, excuteListener);
            mCloudBackupMsgThread.setContext(mContext);
            mCloudBackupMsgThread.start();
            return true;
        }

        @Override
        public Uri insertPdu(String pduPath) {
            Log.d(CLASS_TAG, "insertPdu pduPath = " + pduPath);
            return insertMmsFromPdu(pduPath);
        }
    }


    @Override
    public void onDestroy() {
        Log.d(CLASS_TAG, "onDestroy");
        super.onDestroy();
    }

    /*
     * For favorite mms open.
     */
    private Uri insertMmsFromPdu(String pduPath) {
        Log.d(CLASS_TAG, "insertMmsFromPdu");
        Uri ret = null;
        byte[] pduByteArray = readFileContent(pduPath);
        if (pduByteArray == null) {
            Log.e(CLASS_TAG, "insertMmsFromPdu, pduByteArray == null, return null");
            return null;
        }
        SendReq sendReq = new SendReq();
        MultimediaMessagePdu mmsPdu = (MultimediaMessagePdu) new PduParser(pduByteArray, false)
                .parse();
        if (mmsPdu.getSubject() != null) {
            sendReq.setSubject(mmsPdu.getSubject());
        }
        sendReq.setBody(mmsPdu.getBody());
        PduPersister persister = PduPersister.getPduPersister(this);
        try {
            ret = persister.persist(sendReq, Mms.Draft.CONTENT_URI, true, false, null);
        } catch (MmsException e) {
            e.printStackTrace();
        }
        return ret;
    }

    private byte[] readFileContent(String fileName) {
        try {
            InputStream is = new FileInputStream(fileName);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int len = -1;
            byte[] buffer = new byte[512];
            while ((len = is.read(buffer, 0, 512)) != -1) {
                baos.write(buffer, 0, len);
            }

            is.close();
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getMyMuber() {
        String publicUri = new JoynServiceConfiguration().getPublicUri(mContext);
        String number = "";
        if (!TextUtils.isEmpty(publicUri)) {
            if (publicUri.indexOf('@') != -1) {
                number = publicUri.substring(publicUri.indexOf(':') + 1, publicUri.indexOf('@'));
            } else {
                number = publicUri.substring(publicUri.indexOf(':') + 1);
            }
        }
        Log.d(CLASS_TAG, "getMyAccountNumber = " + number);
        return number;
    }
}
