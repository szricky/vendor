package com.mediatek.op.telephony;

import android.content.Context;
import android.os.SystemProperties;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.internal.telephony.uicc.SIMRecords;

import com.mediatek.common.PluginImpl;

/**
 * plugin impl.
 */
@PluginImpl (interfaceName = "com.mediatek.common.telephony.IServiceStateExt")
public class ServiceStateExtOP18 extends DefaultServiceStateExt {
    Context mContext;
    final String TAG = "ServiceStateExtOP18";
    final String OPERATOR_PLMN = "JIO 4G";
    /**.
     * Constructor
    */
    public ServiceStateExtOP18() {
    }

    /**.
     * Parameter constructor
     * @param context
    */
    public ServiceStateExtOP18(Context context) {
        mContext = context;
        Log.d(TAG, "ServiceStateExtOP18");
    }

    /**.
     * Return String for operator.
     * @return String for operator
     * @param plmn, service state, lac, phoneId
     */
    public String onUpdateSpnDisplayForIms(String plmn, ServiceState ss,
            int lac, int phoneId, Object simRecords1) {
        String eons = null;
        if (SystemProperties.get("ro.mtk_wfc_support").equals("1")
                && ss.getVoiceRegState() == ServiceState.STATE_POWER_OFF
                && simRecords1 != null) {
            SIMRecords simRecords = (SIMRecords)simRecords1;
            try {
                boolean imsRegStatus = ImsManager
                        .getInstance(mContext, phoneId).getImsRegInfo();
                Log.d(TAG, "Ims Registration Status = " + imsRegStatus);
                if (imsRegStatus) {
                    try {
                        eons = (simRecords != null) ? simRecords
                                .getEonsIfExist(ss.getOperatorNumeric(), lac,
                                        true) : null;
                    } catch (RuntimeException ex) {
                        Log.e(TAG, "Exception while getEonsIfExist. " + ex);
                    }
                    if (eons != null) {
                        plmn = eons;
                    } else {
                        String tempPlmn = simRecords.getSIMCPHSOns();
                        if (tempPlmn != null) {
                            plmn = simRecords.getSIMCPHSOns();
                        }
                    }
                }
            } catch (ImsException ex) {
                Log.e(TAG, "Fail to get Ims Status");
            }
        }
        TelephonyManager telephonyManager = (TelephonyManager) mContext
                .getSystemService(Context.TELEPHONY_SERVICE);
        boolean isWFCEnabled = telephonyManager.isWifiCallingEnabled();
        Log.d("ServiceStateExtOP16", "isWFCEnabled = " + isWFCEnabled);
        if (isWFCEnabled) {
            return OPERATOR_PLMN;
        } else {
            return plmn;
        }
    }


   /**
    * Disable IVSR feature.
    */

    /**
     * Return if need disable IVSR.
     * @return if need disable IVSR
     * @param none
     */
    public boolean isNeedDisableIVSR() {
        Log.i(TAG, "Disable IVSR");
        return true;
    }
}
