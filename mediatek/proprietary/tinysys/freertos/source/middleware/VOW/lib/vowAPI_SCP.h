#ifndef _VOW_API_SCP_H
#define _VOW_API_SCP_H

#ifdef __cplusplus
extern "C" {
#endif

int TestingInit_Model (const char *Model);
int TestingInit_Table (int isMultipleCmd);
int setPreLearn (const int loopTimes);
int onTesting (short *pMicBuf1, int *rtnCmdID);
// maximum length should not exceed 2^16-1, ex: 4 secs in 16kHz recording
// i.e. after calling this function 400 times (16kHz), reset should be set as 1, or it may overflow
// the return value will be got if you set reset=1
short DCRemove(short *buff, int len, int reset);

#ifdef __cplusplus
}
#endif

#endif
