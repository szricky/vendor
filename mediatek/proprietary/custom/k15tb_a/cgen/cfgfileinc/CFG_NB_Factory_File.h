#ifndef _CFG_NB_FACTORY_FILE_H
#define _CFG_NB_FACTORY_FILE_H

#define MAX_RECORD 256
typedef struct
{
    unsigned char id[MAX_RECORD];
    unsigned char result[MAX_RECORD];

    unsigned char is_pcba_pass;
    unsigned char total_test_num;
    unsigned char pass_num;
    unsigned char fail_num;
    unsigned char untest_num;
    unsigned char reserved[1024-MAX_RECORD-MAX_RECORD-5];
}NB_Factory_Test_Result_Struct;

#define CFG_FILE_NB_FACTORY_TEST_RESULT_REC_SIZE    sizeof(NB_Factory_Test_Result_Struct)
#define CFG_FILE_NB_FACTORY_TEST_RESULT_REC_TOTAL   1

#endif
